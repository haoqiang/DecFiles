DecFiles v30r59 2021-05-27 
==========================  
 
! 2021-05-25 - Michal Kreps (MR !758, !735)  
   Add new decay file  
   + 11106000 : Bd_pi+pi-pi+pi-Kst=PHSP,DecProdCut  
  
! 2021-05-24 - Florian Reiss (MR !756)  
   Add new decay file  
   + 11574094 : Bd_Dst+munu,D0pi+=HQET2,TightCut  
  
! 2021-05-23 - Eluned Anne Smith (MR !755, !759)  
   Add new decay file  
   + 12123211 : Bu_EtapK,e+e-g=DecProdCut  
  
! 2021-05-20 - Zishuo Yang (MR !754)  
   Add 2 new decay files  
   + 14543221 : Bc_chic1MuNu,Jpsi=BcVegPy,ffWang,JpsiLeptonInAcceptance  
   + 14543222 : Bc_chic2MuNu,Jpsi=BcVegPy,ffWang,JpsiLeptonInAcceptance  
  
! 2021-05-20 - Jan Langer (MR !753)  
   Modify 4 decay files  
   + 11196088 : Bd_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP010  
   + 11196087 : Bd_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP100  
   + 13196054 : Bs_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP010  
   + 13196053 : Bs_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP100  
  
! 2021-05-18 - Asier Pereiro Castro (MR !752)  
   Add new decay file  
   + 13104094 : Bs_KpiKpi=DecProdCut,PhSp  
  
! 2021-05-18 - Alexander Mclean Marshall (MR !751)  
   Add 6 new decay files  
   + 11584000 : B0_Denu,K*enu,Kpi=DecProdCut,TightCut  
   + 12583424 : Bu_D*0enu,D0pi0,Kenu=DecProdCut,TightCut  
   + 12583425 : Bu_D*0pi,D0pi0,Kenu=DecProdCut,TightCut  
   + 12583006 : Bu_D0K,Kenu=DecProdCut,TightCut  
   + 12583023 : Bu_D0enu,Kenu=DecProdCut,TightCut  
   + 12583005 : Bu_D0pi,Kenu=DecProdCut,TightCut,2  
  
! 2021-05-14 - Niladri Sahoo (MR !749)  
   Modify decay file  
   + 13124401 : Bs_phieta,e+e-g=Dalitz,DecProdCut  
  
! 2021-05-14 - Niall Thomas Mchugh (MR !748)  
   Add new decay file  
   + 27163474 : Dst_D0pi,pipipi0=TightCut,Dalitz  
  
! 2021-05-14 - Lorenzo Paolucci (MR !747)  
   Add new decay file  
   + 27163906 : Dst_D0pi,Kpi=TightCut,4  
  
! 2021-05-12 - Albert Bursche (MR !743)  
   Add 6 new decay files  
   + 47100203 : exclu_axion,gg=coherent_starlight_2000MeV  
   + 47100204 : exclu_axion,gg=coherent_starlight_2500MeV  
   + 47100202 : exclu_axion,gg=coherent_starlight_3000MeV  
   + 47100205 : exclu_axion,gg=coherent_starlight_4000MeV  
   + 47100206 : exclu_axion,gg=coherent_starlight_5000MeV  
   + 47100207 : exclu_axion,gg=coherent_starlight_6000MeV  
  
! 2021-05-10 - Hanae Tilquin (MR !740)  
   Add 2 new decay files  
   + 13514041 : Bs_KKmumu=TightCut  
   + 13114082 : Bs_phi3mumu,KK=TightCut  
  
! 2021-05-10 - Andrea Merli (MR !739)  
   Add new decay file  
   + 24104101 : incl_Jpsi,LambdaLambdabar=DecProdCut  
  
! 2021-05-10 - Florian Reiss (MR !738)  
   Add new decay file  
   + 11584032 : Bd_Dst+enu,D0pi+=HQET2,TightCut  
  
! 2021-05-09 - Maarten Van Veghel (MR !737)  
   Add new decay file  
   + 12503200 : Bu_TauNu=DecProdCut  
  
! 2021-05-07 - Marco Santimaria (MR !736)  
   Add new decay file  
   + 39000000 : minbias=BiasedPhiPt300MeV  
  
! 2021-04-30 - Preema Rennee Pais (MR !733)  
   Add 2 new decay files  
   + 12103272 : Bu_Kpipigamma,Kpipi=cocktail,AMPGEN,norm1,TightCut2  
   + 12103273 : Bu_Kpipigamma,Kpipi=cocktail,AMPGEN,val1,TightCut2  
  
! 2021-04-22 - Giulia Tuci (MR !727)  
   Add new decay file  
   + 24104110 : Jpsi_LambdaLambda=TightCut  
  
  
! 2021-04-13 - Jianqiao Wang (MR !718)  
   Add new decay file  
   + 22102007 : D0_Kpi=DecProdCut,D0PtCut=8GeV,epos  
  
! 2021-04-07 - Hanae Tilquin (MR !714)  
   Add 2 new decay files  
   + 13674452 : Bs_D0Kmunu,Kmunu=TightCut  
   + 13574452 : Bs_Dsststmunu,D0=TightCut  
  
! 2021-03-18 - La Wang (MR !701)  
   Add 2 new decay files  
   + 11166030 : Bd_Lcpbarpbarp,pKpi=DecProdCut  
   + 11166081 : Bd_Lcpipip,ForcedDecay  
  
! 2021-03-09 - Yanxi Zhang (MR !693)  
   Add 4 new decay files  
   + 15198100 : Lb_D+D-Lambda,KPiPi,KPiPi,PPi,TightCut  
   + 15196100 : Lb_D0D0Lambda,KPi,KPi,PPi,TightCut  
   + 16166142 : Xib0_D+Lambda0pi-,Kpipi,ppi=phsp,TightCut  
   + 16165132 : Xib_D0Lambda0pi-,Kpi,ppi=phsp,TightCut  
  
