DecFiles v30r71 2022-03-10 
==========================  
 
! 2022-03-10 - Zhenhong Wu (MR !990)  
   Add 2 new decay files  
   + 26144101 : Pcs4254,JpsiLambda=DecProdCut,InAcc  
   + 26144100 : incl_X_JpsiLambda,mumu=phsp,DecProdCut  
  
! 2022-03-09 - La Wang (MR !989)  
   Add 4 new decay files  
   + 11196025 : Bd_LambdacLambdac,Exclusive,pKpi=DecProdCut_pCut1600MeV  
   + 11196026 : Bd_XicXic,Exclusive,pKpi=DecProdCut_pCut1600MeV  
   + 13196090 : Bs_LambdacLambdac,Exclusive,pKpi=DecProdCut_pCut1600MeV  
   + 13196091 : Bs_XicXic,Exclusive,pKpi=DecProdCut_pCut1600MeV  
  
! 2022-03-07 - Saul Lopez Solino (MR !987)  
   Add 3 new decay files  
   + 15586098 : Lb_Lambdac2595enu,Lambdac2595_pipiLc,Lc_pKpi  
   + 15576099 : Lb_Lambdac2595munu,Lambdac2595_pipiLc,Lc_pKpi  
   + 15565097 : Lb_Lambdac2595taunu,Lambdac2595_pipiLc,Lc_pKpi  
  
! 2022-03-03 - Antonio Romero Vidal (MR !983)  
   Add 3 new decay files  
   + 11466421 : Bd_D-3piX,Kpipi=TightCut  
   + 12467401 : Bu_D-3piX,Kpipi=TightCut  
   + 21103000 : b_Dm3piInclBkg,Kpi=TightCut  
  
! 2022-02-28 - Haoqiang Zhao (MR !982)  
   Add 2 new decay files  
   + 11164094 : Bd_Lcpbar,pKpi=DecProdCut  
   + 13164094 : Bs_Lcpbar,pKpi=DecProdCut  
  
! 2022-02-25 - Maximilien Chefdeville (MR !981)  
   Add 2 new decay files  
   + 11142213 : Bd_Jpsietap,mm=TightCut  
   + 13142213 : Bs_Jpsietap,mm=TightCut  
  
! 2022-02-25 - Maximilien Chefdeville (MR !980)  
   Add new decay file  
   + 11142402 : Bd_Jpsipi,mm=TightCut  
  
! 2022-02-23 - Jingyi Xu (MR !979)  
   Add new decay file  
   + 26143018 : Pc4150,Jpsip=DecProdCut,InAcc  
  
! 2022-02-17 - Hanae Tilquin (MR !978)  
   Add new decay file  
   + 13574052 : Bs_Dsststmunu,D+=DecProdCut  
  
! 2022-02-17 - Alice Biolchini (MR !977)  
   Add new decay file  
   + 11224400 : Bd_K1ee,Kpipi0=mK1270,DecProdCut  
  
! 2022-02-16 - Hanae Tilquin (MR !976)  
   Add new decay file  
   + 11694052 : Bd_DDKst,munu,munu=DecProdCut  
  
! 2022-02-10 - Gary Robertson (MR !973)  
   Add 3 new decay files  
   + 26196046 : Pcc4175,LcD+,pkpi=TightCut,InAcc  
   + 26195071 : Pcc4175,LcD0,pkpi=TightCut,InAcc  
   + 26196045 : Pcc4350,LcDst+,pkpi=TightCut,InAcc  
  
! 2022-02-08 - Harry Victor Cliff (MR !971)  
   Add 6 new decay files  
   + 11154020 : Bd_Jpsirho0,ee=DecProdCut  
   + 11154021 : Bd_psi2Srho0,eepipi=DecProdCut  
   + 13154020 : Bs_Jpsif0,ee=CPV,980,DecProdCut  
   + 13124041 : Bs_f0ee=MS,DecProdCut  
   + 13154021 : Bs_psi2Sf0,ee=980,DecProdCut  
   + 12153030 : Bu_psi2SPi,ee=DecProdCut  
  
! 2022-02-07 - Daniel Cervenkov (MR !970)  
   Add new decay file  
   + 27165008 : Dst_D0pi,KKpipi=DecProdCut,TightCuts,AMPGENv2  
  
! 2022-02-01 - Hangyi Wu (MR !968)  
   Add 2 new decay files  
   + 12265063 : Bu_D+pi-pi-,Kpipi=cocktail,DecProdCut  
   + 12265065 : Bu_Dst+pi-pi-,D0pi,Kpi=cocktail,DecProdCut  
  
! 2022-01-28 - Maximilien Chefdeville (MR !967)  
   Add new decay file  
   + 13102264 : Bs_Phigamma=TightCut,gam_PTabove1.8  
  
! 2022-01-26 - Alessandro Scarabotto (MR !966)  
   Add 12 new decay files  
   + 27185202 : Dst_D0pi,KKeta,eeg=DecProdCut  
   + 27163479 : Dst_D0pi,KKeta,gg=DecProdCut  
   + 27185205 : Dst_D0pi,KKpi0,eeg=DecProdCut  
   + 27163482 : Dst_D0pi,KKpi0,gg=DecProdCut  
   + 27185200 : Dst_D0pi,Kpieta,eeg=DecProdCut  
   + 27163477 : Dst_D0pi,Kpieta,gg=DecProdCut  
   + 27185203 : Dst_D0pi,Kpipi0,eeg=DecProdCut  
   + 27163480 : Dst_D0pi,Kpipi0,gg=DecProdCut  
   + 27185201 : Dst_D0pi,pipieta,eeg=DecProdCut  
   + 27163478 : Dst_D0pi,pipieta,gg=DecProdCut  
   + 27185204 : Dst_D0pi,pipipi0,eeg=DecProdCut  
   + 27163481 : Dst_D0pi,pipipi0,gg=DecProdCut  
  
! 2022-01-26 - Hanae Tilquin (MR !965)  
   Add new decay file  
   + 13774072 : Bs_DKpimunu,munuCocktail=DecProdCut  
  
! 2022-01-26 - Jolanta Brodzicka (MR !964)  
   Modify decay file  
   + 27263479 : Dst_D0pi,Kpieta=TightCut,tighter,Coctail  
  
! 2022-01-24 - Albert Bursche (MR !961)  
   Add new decay file  
   + 20462000 : Zcharm=mumu,charged,InAcc  
  
