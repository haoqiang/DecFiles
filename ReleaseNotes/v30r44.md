!========================= DecFiles v30r44 2020-04-02 =======================  
  
! 2020-04-02 - Antonio Romero Vidal (MR !451)  
   Add 6 new decay files  
   + 11196017 : Bd_D-Ds+,Kpipi,pipipi=DDalitz,DecProdCut  
   + 11466400 : Bd_D03piX,Kpi=TightCut  
   + 13466400 : Bs_D03piX,Kpi=TightCut  
   + 12465400 : Bu_D03piX,Kpi=TightCut  
   + 12195048 : Bu_D0Ds-,Kpi,pipipi=DDalitz,DecProdCut  
   + 22102000 : b_D03piInclBkg,Kpi=TightCut  
  
! 2020-03-31 - Mick Mulder (MR !449)  
   Add new decay file  
   + 16114540 : Xib0_Xi0mumu,Lambdapi=phsp,DecProdCut  
  
! 2020-03-30 - Qiaohong Li (MR !448)  
   Add new decay file  
   + 11144432 : Bd_Jpsieta,mm,pipipi=DecProdCut  
  
! 2020-03-30 - Qiaohong Li (MR !447)  
   Add new decay file  
   + 11144440 : Bd_Jpsia10,mm,pipipi=DecProdCut  
  
! 2020-03-26 - Pablo Baladron Rodriguez (MR !446)  
   Add 16 new decay files  
   + 11104058 : Bd_KKKK=MassWindowCut,DecProdCut  
   + 11104057 : Bd_KKKpim=MassWindowCut,DecProdCut  
   + 11104056 : Bd_KKpipi=MassWindowCut,DecProdCut  
   + 11104055 : Bd_Kppipipi=MassWindowCut,DecProdCut  
   + 11104054 : Bd_pipipipi=MassWindowCut,DecProdCut  
   + 13104056 : Bs_KKKK=MassWindowCut,DecProdCut  
   + 13104055 : Bs_KKKpim=MassWindowCut,DecProdCut  
   + 13104054 : Bs_KKpipi=MassWindowCut,DecProdCut  
   + 13104053 : Bs_Kppipipi=MassWindowCut,DecProdCut  
   + 13104052 : Bs_pipipipi=MassWindowCut,DecProdCut  
   + 12105156 : Bu_KsKKK=MassWindowCut,DecProdCut  
   + 12105155 : Bu_KsKKpim=MassWindowCut,DecProdCut  
   + 12105154 : Bu_KsKKpip=MassWindowCut,DecProdCut  
   + 12105153 : Bu_KsKmpipi=MassWindowCut,DecProdCut  
   + 12105152 : Bu_KsKppipi=MassWindowCut,DecProdCut  
   + 12105151 : Bu_Kspipipi=MassWindowCut,DecProdCut  
  
! 2020-03-26 - Carmen Giugliano (MR !445)  
   Add 2 new decay files  
   + 13563001 : Bs_DsTauNu,KKPi,PiPiPi=TightCut,tauola  
   + 13266069 : Bs_Dspipipi,KKpi=TightCut,DsPt1400  
  
! 2020-03-25 - Michal Kreps (MR !444)  
   - Use gitlab API to obtain list of cuts from repository 
     
  
! 2020-03-24 - Michel De Cian (MR !443)  
   Add 2 new decay files  
   + 11873040 : Bd_D0Xmunu,D0=cocktail,TightCut,ForB2RhoMuNu  
   + 12513011 : Bu_phimunu=TightCut,BToVlnuBall  
  
! 2020-03-21 - Chen Chen (MR !441)  
   Add new decay file  
   + 11198081 : Bd_DDKpi,Kpipi=DecProdCut  
  
! 2020-03-20 - Gary Robertson (MR !440)  
   Add 12 new decay files  
   + 26197071 : Pc4400,Sigma_c++D-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26196041 : Pc4400,Sigma_c++D0bar,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197072 : Pc4500,Sigma_c++Dst-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197073 : Pc4800,Sigma_c++D-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26196042 : Pc4800,Sigma_c++D0bar,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197074 : Pc4800,Sigma_c++Dst-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197075 : Pc5200,Sigma_c++D-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26196043 : Pc5200,Sigma_c++D0bar,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197076 : Pc5200,Sigma_c++Dst-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197077 : Pc5600,Sigma_c++D-,Lcpi,pkpi=DecProdCut,InAcc  
   + 26196044 : Pc5600,Sigma_c++D0bar,Lcpi,pkpi=DecProdCut,InAcc  
   + 26197078 : Pc5600,Sigma_c++Dst-,Lcpi,pkpi=DecProdCut,InAcc  
  
! 2020-03-19 - Michel De Cian (MR !439)  
   Add 4 new decay files  
   + 11873041 : Bd_D+Xmunu,D+=cocktail,TightCut,ForB2RhoMuNu  
   + 11873040 : Bd_D0Xmunu,D0=cocktail,TightCut,ForB2RhoMuNu  
   + 12873041 : Bu_D+Xmunu,D+=cocktail,TightCut,ForB2RhoMuNu  
   + 12873040 : Bu_D0Xmunu,D0=cocktail,TightCut,ForB2RhoMuNu  
  
! 2020-03-18 - Pavel Krokovny (MR !438)  
   Add 3 new decay files  
   + 11246130 : Bd_ccKS,Jpsipipi,mm=TightCut  
   + 11246030 : Bd_ccKst,Jpsipipi,mm=TightCut  
   + 12247130 : Bu_ccKst,Jpsipipi,mm=TightCut  
  
! 2020-03-18 - Andrii Usachov (MR !437)  
   Add 4 new decay files  
   + 18302009 : incl_etab,2h=UpsilonDaughtersInLHCb  
   + 18304009 : incl_etab,4h=UpsilonDaughtersInLHCb  
   + 18306009 : incl_etab,6h=UpsilonDaughtersInLHCb  
   + 24104100 : incl_etac,KsKpi,pipi=TightCut  
  
! 2020-03-13 - Jeremy Peter Dalseno (MR !436)  
   Add 2 new decay files  
   + 11104053 : Bd_pi+pi-pi+pi-=DecProdCut,PHSP,Charmless  
   + 11102412 : Bd_rho+rho-=DecProdCut,eqPol  
  
! 2020-03-09 - Jeremy Peter Dalseno (MR !434)  
   Add 1 new decay file  
   + 14103121 : Bc_Kspi=BcVegPy,DecProdCut  
   Cleanup of decay file (no functionality change)  
   + 14103101 : Bc_KsK=BcVegPy,DecProdCut  
  
! 2020-03-07 - Zihan Wang (MR !433)  
   Add 2 new decay files  
   + 12145240 : Bu_chic1KKK,mm=DecProdCut  
   + 12145250 : Bu_chic2KKK,mm=DecProdCut  
   Modify nickname of 2 decay files to reflect what is actualy done
   + 12145260 : Bu_chic1phiK,mm=DecProdCut  
   + 12145270 : Bu_chic2phiK,mm=DecProdCut  
  
! 2020-03-06 - Jeremy Peter Dalseno (MR !430)  
   Add 4 new decay files  
   + 11104505 : Bd_etapKS,etapi+pi-,gg=DecProdCut,CPV,PHSP  
   + 11106501 : Bd_etapKS,etapi+pi-,pi+pi-pi0=DecProdCut,CPV,PHSP  
   + 11104313 : Bd_etapKs,pi+pi-g=DecProdCut,CPV,PHSP  
   + 12105421 : Bu_etapK+,etapi+pi-,pi+pi-pi0=DecProdCut,PHSP  
  
! 2020-03-05 - Jacco Andreas De Vries (MR !428)  
   Add new decay file  
   + 26103091 : Xic_pKpi=phsp,TightCutv2  
  
! 2020-03-04 - Jacco Andreas De Vries (MR !427)  
   Add new decay file  
   + 25103064 : Lc_pKpi=phsp,TightCutv2  
  
! 2020-03-03 - Vitalii Lisovskyi (MR !426)  
   Add new decay file  
   + 13114065 : Bs_ppbarmumu=TightCut  
  
! 2020-02-29 - Vitalii Lisovskyi (MR !425)  
   Add 3 new decay files  
   + 12145067 : Bu_KOmegaJpsi,mm=LSFLAT,DecProdCut  
   + 12145068 : Bu_KOmegaJpsi,mm=PHSP,DecProdCut  
   + 12145069 : Bu_chic1K,Jpsimumu=DecProdCut  
  
! 2020-02-28 - Carmen Giugliano (MR !424)  
   Add new decay file  
   + 11264070 : Bd_D-PiPiPi+,KKPi=DecProdCut  
  
