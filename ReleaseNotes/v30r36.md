
!========================= DecFiles v30r36 2019-09-16 =======================  
  
! 2019-09-12 - Giulia Tuci (MR !354)  
   Add new decay file  
   + 27165900 : Dst_D0pi,KSKS=TightCut,1  
  
! 2019-09-06 - Maurizio Martinelli (MR !353)  
   Add 8 new decay files  
   + 12135107 : Bu_Chic1K,KSK+pi-=DecProdCut  
   + 12135106 : Bu_Chic1K,KSK-pi+=DecProdCut  
   + 12135105 : Bu_Etac2SK,KSK+pi-=DecProdCut  
   + 12135104 : Bu_Etac2SK,KSK-pi+=DecProdCut  
   + 12135103 : Bu_EtacK,KSK+pi-=DecProdCut  
   + 12135102 : Bu_EtacK,KSK-pi+=DecProdCut  
   + 12135101 : Bu_JpsiK,KSK+pi-=DecProdCut  
   + 12135100 : Bu_JpsiK,KSK-pi+=DecProdCut  
  
! 2019-09-04 - Donal Hill (MR !352)  
   Add 2 new decay files  
   + 12165114 : Bu_D0K,KSK+pi-=DecProdCut  
   + 12165162 : Bu_D0pi,KSK+pi-=DecProdCut  
  
! 2019-08-30 - Jan-Marc Basels (MR !350)  
   Add 4 new decay files  
   + 13584000 : Bs_Dsenu,phienu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574092 : Bs_Dsmunu,phimunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13584200 : Bs_Dsstenu,Dsgamma,phienu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
   + 13574212 : Bs_Dsstmunu,Dsgamma,phimunu=DecProdCut,HighVisMass,EvtGenDecayWithCut  
  
! 2019-08-29 - Lorenzo Capriotti (MR !349)  
   Add 2 new decay files  
   + 12145403 : Bu_KOmegaJpsi,pi0pi+pi-=PHSP,mm=TightCut2,JpsiOmegaCUT  
   + 12145404 : Bu_KOmegaJpsi,pi0pi+pi-=PHSP,mm=TightCut2,JpsiOmegaTIGHTCUT  
  
! 2019-08-28 - Razvan-Daniel Moise (MR !348)  
   Add new decay file  
   + 12123445 : Bu_Kstee,Kpi0=btosllball05,DecProdCut  
  
! 2019-08-21 - Matthew Scott Rudolph (MR !346)  
   Add 2 new decay files  
   + 13516000 : Bs_phitaumu,3pi=PHSP,TightCut,tauola5  
   + 13516400 : Bs_phitaumu,3pipi0=PHSP,TightCut,tauola8  
  
