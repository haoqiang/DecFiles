# EventType: 11364401
#
# Descriptor: {[ B0 -> ( D*- -> ( anti-D0 ->  K+ pi- )  pi- ) a_1+  ]cc}
#
# NickName: Bd_Dsta1,D0pi,Kpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# tightCut = gen.TightCut
# tightCut.Decay = "[(Beauty & LongLived) --> ^( D*(2010)- => pi- ^( D~0 => K+ pi- ) ) pi+ pi- pi+ ... ]CC"
# tightCut.Cuts = {
#     "(Beauty & LongLived)" : "good_B",
#     "[D*(2010)+]cc" : "good_Dstar",
#     "[D0]cc" : "good_D0",
# }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV, GeV",
#     "inAcc = ( GPZ > 0 ) & ( GPT > 40 * MeV ) & ( GP > 1.0 * GeV ) & in_range ( 1.8, GETA, 5.0 ) & in_range ( 0.005, GTHETA, 0.400 )",
#     "nPi = GCOUNT(('pi+' == GABSID) & inAcc, HepMC.descendants)",
#     "nK  = GCOUNT(('K-'  == GABSID) & inAcc, HepMC.descendants)",
#     "good_slow_pion = ('pi+' == GABSID) & (GPT >  40 * MeV) & inAcc",
#     "good_Dstar     = ('D*(2010)+' == GABSID) & GCHILDCUT(good_slow_pion, 'Charm => Charm ^(pi+|pi-)')",
#     "good_D0_pion   = ('pi+' == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0_kaon   = ('K+'  == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0        = ('D0'  == GABSID) & (GPT > 1.5 * GeV) & ( GP > 15. * GeV ) & GCHILDCUT(good_D0_kaon, 'Charm => ^(K+|K-) (pi+|pi-)') & GCHILDCUT(good_D0_pion, 'Charm => (K+|K-) ^(pi+|pi-)')",
#     "good_B         = (nK >= 1) & (nPi >= 5)",
# ]
# EndInsertPythonCode
#
# Documentation: B0 -> D*- a_1+, where D*- -> anti-D0 pi-, anti-D0 -> K+ pi-, and where a_1+ decays to a cocktail of relevant final states.
# Background for B2XTauNu Analysis
# EndDocumentation
#
# CPUTime: <1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Donal Hill, Luke Scantlebury-Smead
# Email: donal.hill@cern.ch, luke.george.scantlebury.smead@cern.ch
# Date: 20200113
#
#
# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias       mymainD*-       D*-
Alias       mymainD*+       D*+
ChargeConj       mymainD*-       mymainD*+
#
Alias       mymainanti-D0       anti-D0
Alias       mymainD0       D0
ChargeConj       mymainanti-D0       mymainD0
#
Alias       mya_1+       a_1+
Alias       mya_1-       a_1-
ChargeConj       mya_1+       mya_1-
#
Alias       myrho0       rho0
#
Alias       myrho+       rho+
Alias       myrho-       rho-
ChargeConj       myrho+       myrho-
#
#
Decay B0sig
0.011    mymainD*-   mya_1+     SVV_HELAMP   0.2   0.0   0.866   0.0   0.458   0.0;
Enddecay
CDecay anti-B0sig
#
#
Decay mymainD*-
0.677    mymainanti-D0  pi-     VSS   ;
Enddecay
CDecay mymainD*+
#
#
Decay mymainanti-D0
0.0389   K+  pi-     PHSP   ;
Enddecay
CDecay mymainD0
#
#
Decay mya_1+
0.492    myrho0  pi+     VVS_PWAVE   1.0   0.0   0.0   0.0   0.0   0.0;
0.508    myrho+  pi0     VVS_PWAVE   1.0   0.0   0.0   0.0   0.0   0.0;
Enddecay
CDecay mya_1-
#
#
Decay myrho0
1.0   pi+  pi-     VSS   ;
Enddecay
#
#
Decay myrho+
1.0   pi+  pi0     VSS   ;
Enddecay
CDecay myrho-
#
#
End
