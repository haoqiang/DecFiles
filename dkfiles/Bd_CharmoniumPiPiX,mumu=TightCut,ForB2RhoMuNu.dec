# EventType: 11444001
#
# Descriptor: [B0 -> (Charmonium -> mu+ mu- X) pi+ pi- X]cc 
# 
# NickName: Bd_CharmoniumPiPiX,mumu=TightCut,ForB2RhoMuNu
# 
# Cuts: LoKi::GenCutTool/TightCut 
# 
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# SignalFilter = gen.TightCut
# SignalFilter.Decay = "^( (Beauty & LongLived) --> ( ( J/psi(1S) | psi(2S) ) --> mu+ mu- ...) pi+ pi- ...)"
# SignalFilter.Preambulo += [
#   "from GaudiKernel.SystemOfUnits import  GeV",
#   "inAcc                = in_range ( 0.005 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )", 
#   "muCuts               = (0 < GNINTREE ( ('mu-' == GABSID ) & (GP > 5 * GeV) &  (GPT > 1.2 * GeV)  & inAcc))",
#   "piPlusCuts           = (0 < GNINTREE ( ('pi+' == GID ) & (GP > 1.5 * GeV) & (GPT > 0.35 * GeV)  & inAcc ))",
#   "piMinusCuts          = (0 < GNINTREE ( ('pi-' == GID ) & (GP > 1.5 * GeV) & (GPT > 0.35 * GeV)  & inAcc ))",
#   "piMaxPT              = (GMAXTREE( GPT, ('pi+' == GABSID) & inAcc & (GP > 1.5 * GeV)) > 0.85 * GeV )",
#   "piMaxP               = (GMAXTREE( GP, ('pi+' == GABSID) & inAcc & (GPT > 0.35 * GeV)) > 4.5 * GeV )",
#   "allcuts              = ( muCuts & piPlusCuts & piMinusCuts & piMaxPT & piMaxP )"
#   ]
# SignalFilter.Cuts =  { "Beauty" : "allcuts" }
# EndInsertPythonCode
#
# Documentation: B0 -> J/psi pi+ pi- X events, with cuts optimised for B -> rho mu nu analysis.
# EndDocumentation 
# 
# PhysicsWG: B2SL 
# Tested: Yes 
# CPUTime:< 1min
# Responsible: Michel De Cian
# Email: michel.de.ciann@cern.ch 
# Date: 20210922
#
Define PKHplus  0.159
Define PKHzero  0.775
Define PKHminus 0.612
Define PKphHplus  1.563
Define PKphHzero  0.0
Define PKphHminus 2.712
#
Alias MyJ/psi J/psi 
ChargeConj MyJ/psi MyJ/psi 
# 
Alias Mychi_c1 chi_c1 
ChargeConj Mychi_c1 Mychi_c1 
# 
Alias Mychi_c2 chi_c2 
ChargeConj Mychi_c2 Mychi_c2 
#
Alias Mychi_c0 chi_c0 
ChargeConj Mychi_c0 Mychi_c0 
# 
Alias Mypsi(2S) psi(2S) 
ChargeConj Mypsi(2S) Mypsi(2S) 
#
Alias      MyKst-      K*- 
Alias      MyKst+      K*+ 
ChargeConj MyKst-      MyKst+
#
Alias      MyKst0       K*0 
Alias      Myanti-Kst0  anti-K*0 
ChargeConj MyKst0       Myanti-Kst0
#
#
Alias      K1(1270)		K_10 
Alias      Myanti-K1(1270)  	anti-K_10 
ChargeConj K1(1270)       	Myanti-K1(1270)
#
Alias      K*0(1430)		K_0*0
Alias      Myanti-K*0(1430)	anti-K_0*0
ChargeConj K*0(1430)		Myanti-K*0(1430)
#
Alias      K*0(1430)+		K_0*+
Alias      K*0(1430)-		K_0*-
ChargeConj K*0(1430)+		K*0(1430)-
#
Decay Mychi_c1
0.3430  MyJ/psi	gamma				VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
#
Decay Mychi_c2
0.1900  MyJ/psi	gamma				PHSP ;
Enddecay
#
Decay Mychi_c0
0.0140  MyJ/psi	gamma				SVP_HELAMP 1.0 0.0 1.0 0.0 ;
Enddecay
#
Decay MyJ/psi
1.00000 mu+	mu-				PHOTOS VLL ;
Enddecay
#
Decay Mypsi(2S)
0.0080  mu+ mu-					PHOTOS VLL;
0.3467  MyJ/psi    pi+        pi-		PHOTOS VVPIPI ; 
0.0337  MyJ/psi    eta                          PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ; 
0.0979  Mychi_c0   gamma                        PHSP ; 
0.0975  Mychi_c1   gamma                        VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ; 
0.0952  Mychi_c2   gamma                        PHSP ; 
Enddecay
#
Decay K1(1270)
0.2800  rho0		K0                      VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.1867  K*0(1430)+	pi- 			PHSP;
0.1067  MyKst+   	pi-                     VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.1100  omega K0                          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Myanti-K1(1270)
#
Decay K*0(1430)+
0.31	K0	pi+				PHSP;
Enddecay
CDecay K*0(1430)-
#
Decay MyKst+
0.66	K0	pi+				VSS;
Enddecay
CDecay MyKst-
#
Decay MyKst0
0.6667	K+	pi-				VSS;
Enddecay
CDecay Myanti-Kst0
#
Decay B0sig
0.001300	MyJ/psi 	K1(1270) 		SVV_HELAMP 0.5 0.0 1.0 0.0 0.5 0.0 ; 
0.000800	MyJ/psi 	MyKst+	pi- 		PHSP ;
0.000590 	Mypsi(2S) 	MyKst0 			SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ; 
0.000320 	Mychi_c1 	pi- 	pi+ 	K0 	PHSP ;
Enddecay 
CDecay anti-B0sig 
# 
End
#
