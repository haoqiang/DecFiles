# EventType: 11196083
#
# Descriptor: [B0 -> (D*(2010)- -> pi- (D~0 -> K+ K-)) ((D+ -> pi+ pi+ K-))]cc
#
#
# NickName: Bd_DstD,D0KK=DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation:  B0->D*D withOUT CPV, D*->D0(->KK)pi and Dalitz decay model for D decay. 
# Daughters in LHCb.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Stefania Vecchi
# Email: stefania.vecchi@cern.ch
# Date: 20170707
# CPUTime: < 3 sec

# -------------------------
# THEN DEFINE THE D+ AND D-
# -------------------------
Alias      MyD+  D+
Alias      MyD-  D-
ChargeConj MyD+  MyD-

# -------------------------
# THEN DEFINE THE D*+ AND D*-
# -------------------------
Alias      MyD*+       D*+
Alias      MyD*-       D*-
ChargeConj MyD*+       MyD*-

# -------------------------
# THEN DEFINE THE D0 AND D~0
# -------------------------
Alias       MyD0        D0
Alias       Myanti-D0   anti-D0
ChargeConj  MyD0        Myanti-D0

# ---------------
# DECAY OF THE B0
# ---------------
Decay B0sig
	1.000         MyD*-    MyD+ 			PHSP;
Enddecay
CDecay anti-B0sig

# ---------------
# DECAY OF THE D*-
# ---------------
Decay MyD*-
  1.000        Myanti-D0 pi- 				VSS;
Enddecay
CDecay MyD*+

# ---------------
# DECAY OF THE D~0
# ---------------
Decay Myanti-D0
  1.000        K+        K-                    PHSP;
Enddecay
CDecay MyD0

# ---------------
# DECAY OF THE D+
# ---------------
Decay MyD+
  1.000  pi+ pi+ K-                 D_DALITZ;
Enddecay
CDecay MyD-
#
End

