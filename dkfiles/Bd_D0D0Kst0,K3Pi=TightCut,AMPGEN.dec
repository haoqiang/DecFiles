# EventType: 11198008
# NickName: Bd_D0D0Kst0,K3Pi=TightCut,AMPGEN
# Descriptor: (B0 -> [([D0]cc -> K- pi+ pi+ pi-)]CC [([D~0]cc -> K+ pi-)]CC (K*(892)0 -> K+ pi-)) || (B~0 -> [([D0]cc -> K- pi+ pi+ pi-)]CC [([D~0]cc -> K+ pi-)]CC (K*(892)~0 -> K- pi+))
# 
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
##
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool( LoKi__GenCutTool , 'TightCut' )
##
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = ' ^(B0 ==> ^[([D0]cc => K- pi+ pi+ pi-)]CC ^[([D~0]cc => K+ pi-)]CC ^(K*(892)0 => K+ pi-)) || ^(B~0 ==> ^[([D0]cc => K- pi+ pi+ pi-)]CC ^[([D~0]cc => K+ pi-)]CC ^(K*(892)~0 => K- pi+)) '
# 
# tightCut.Preambulo += [
#     'inAcc = in_range (0.005, GTHETA , 0.400) & in_range (1.8 , GETA , 5.2)',
#     'goodFinalState = (GPT > 80 * MeV) & inAcc',
#     'goodB = (GPT > 4000 * MeV)',
#     'goodD0daughter = goodFinalState & (GP > 800 * MeV)',
#     'goodKstdaughter = goodFinalState & (GP > 1800 * MeV)',
#     'goodD0pi2b =  GDECTREE ("[D0 ==> K- pi+]CC") & (GNINTREE( ("pi-"==GABSID) & goodD0daughter) == 1 ) & (GNINTREE( ("K-"==GABSID) & goodD0daughter) == 1 )',
#     'goodD0pi4b = GDECTREE ("[D0 ==> K- pi+ pi+ pi-]CC") & (GNINTREE( ("pi-"==GABSID) & goodD0daughter) == 3 ) & (GNINTREE( ("K-"==GABSID) & goodD0daughter) == 1 )',
#     'goodD0pi = ( goodD0pi2b | goodD0pi4b )',
#     'goodKst = (GNINTREE( ("pi+"==GABSID) & goodKstdaughter) > 0.5) & (GNINTREE( ("K-"==GABSID) & goodKstdaughter) > 0.5)'
# ]
##
# tightCut.Cuts = {
#     '[B0]cc'  : 'goodB' , 
#     '[D0]cc'  : 'goodD0pi', 
#     '[K*(892)0]cc' : 'goodKst'
# }
# EndInsertPythonCode
#
# Documentation:  K*0 forced to K+ pi-, D0 and D~0 decay into Kpi or K3pi seperately, decay products in acceptance and PT, P cuts applied. PT cut also applied to B0.
#    Decay file for B0 -> D0 D~0 K*0
# EndDocumentation
# 
# Date:   20210210
# Responsible: Jake Amey
# Email: wq20892@bristol.ac.uk
# PhysicsWG: B2OC
# CPUTime: < 1 min
# Tested: Yes

Alias My_D0_Kpi    D0
Alias My_anti-D0_Kpi    anti-D0
Alias My_D0_K3pi    D0
Alias My_anti-D0_K3pi    anti-D0
Alias My_K*0    K*0
Alias My_anti-K*0    anti-K*0

ChargeConj My_D0_Kpi   My_anti-D0_Kpi 
ChargeConj My_D0_K3pi   My_anti-D0_K3pi
ChargeConj My_K*0  My_anti-K*0

Decay My_D0_Kpi
  1.000 K- pi+   PHSP;
Enddecay
CDecay My_anti-D0_Kpi

Decay My_D0_K3pi
  1.000 K- pi+ pi+ pi-   LbAmpGen DtoKpipipi_v2;
Enddecay
CDecay My_anti-D0_K3pi

Decay My_K*0
  1.000 K+ pi-  VSS;
Enddecay
CDecay My_anti-K*0

Decay B0sig
  0.5  My_D0_Kpi My_anti-D0_K3pi My_K*0 PHSP;
  0.5  My_D0_K3pi My_anti-D0_Kpi My_K*0 PHSP;
Enddecay
CDecay anti-B0sig

End
