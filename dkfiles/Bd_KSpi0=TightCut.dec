# EventType: 11102521
#
# Descriptor: [B0 -> (K_S0 -> pi+ pi-) (pi0 -> gamma gamma)]cc
#
# NickName: Bd_KSpi0=TightCut
#
# Cuts: 'LoKi::GenCutTool/TightCut'
#
# CPUTime: < 1 min
#
# Documentation: B0 to KS pi0 with KS => pi+ pi- and pi0 -> gamma gamma, tight cuts on acceptance and kinematics 
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# Responsible: Zishuo Yang
# Email: zishuo.yang@cern.ch
# Date: 20181116
#
# InsertPythonCode:
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool( LoKi__GenCutTool, 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[ B0 => ^(KS0 => ^pi+ ^pi-) ^(pi0 -> ^gamma ^gamma) ]CC'
#tightCut.Preambulo += [
#   "GVZ = LoKi.GenVertices.PositionZ() ",
#   "from LoKiCore.functions import in_range" ,
#   "from GaudiKernel.SystemOfUnits import GeV, MeV, millimeter" ,
#   "inAcc   = (in_range(0.005, GTHETA, 0.400))" ,
#   "inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5" ,
#   "inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5" ,
#   "goodKS  = (GFAEVX(abs(GVZ),0) < 700*mm) & (GP > 6*GeV) & (GPT > 350*MeV) ",
#   "goodPi0 = (GPT > 2500*MeV) & (GP > 4*GeV) "
# ]
#tightCut.Cuts = {
#   "[KS0]cc"   : "goodKS",
#   "[pi+]cc"   : "inAcc",
#   "[pi0]cc"   : "goodPi0",
#   "gamma"     : "inEcalX & inEcalY"
#   }
# EndInsertPythonCode
#

Alias   MyKs    K_S0
ChargeConj MyKs MyKs
Alias   Mypi0   pi0
ChargeConj Mypi0    Mypi0


Decay B0sig
 1.000  MyKs    Mypi0   PHSP;
Enddecay
CDecay anti-B0sig

Decay MyKs
 1.000  pi+ pi- PHSP;
Enddecay

Decay Mypi0
 1.000  gamma   gamma   PHSP;
Enddecay

End

