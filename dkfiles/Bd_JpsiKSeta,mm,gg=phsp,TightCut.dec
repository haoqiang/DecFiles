# EventType: 11144501
#
# Descriptor: [B0 -> (J/psi(1S) -> mu+ mu-) (KS0 -> pi+  pi-) (eta -> gamma gamma) ]cc
#
# NickName: Bd_JpsiKSeta,mm,gg=phsp,TightCut 
#
# Cuts: LoKi::GenCutTool/TightCut
# PolarizedLambdab: no 
#
# Documentation: B0 to three-body J/psi KS eta with J/psi to dimuons and eta to gamma gamma.
# EndDocumentation
#
# PhysicsWG: B2Ch 
# CPUTime: 1 min
# Tested: Yes
# Responsible: Michal Kreps 
# Email:  michal.kreps@cern.ch
# Date: 20210804
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[ B0  ==>  ^(J/psi(1S) => ^mu+ ^mu-) ^(eta -> ^gamma ^gamma) ^(KS0 => pi+ pi-) ]CC'
# tightCut.Cuts      =    {
#     'gamma'           : ' goodGamma ' ,
#     '[mu+]cc'         : ' goodMuon  ' , 
#     'J/psi(1S)'       : ' goodPsi   ' ,
#     'KS0'             : ' goodKS0   ' ,
#     'eta'             : ' goodEta   ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, MeV, GeV                             ' ,
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) & in_range(1.8, GETA, 5.2)             ' , 
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5                                             ' , 
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5                                             ' , 
#     'inEcalHole = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 ) ' ,
#     'goodMuon  = ( GPT > 490  * MeV ) & ( GP > 5.4 * GeV )             & inAcc              ' , 
#     'goodGamma = ( 0 < GPZ ) & ( 140 * MeV < GPT ) & inEcalX & inEcalY & ~inEcalHole        ' ,
#     'goodPsi   = in_range ( 1.8 , GY , 4.5 )                                                ' ,
#     "goodKS0 = (GFAEVX(abs(GVZ),0) < 2500.0 * millimeter) & (GINTREE ( ( 'pi+' == GABSID ) & ( GP > 1.3 * GeV ) )) & (GINTREE ( ( 'pi+' == GABSID ) & ( GP > 1.3 * GeV ) ))",
#     'goodEta   = ( GPT > 590  * MeV )                                                       ']
#
# EndInsertPythonCode
#
Alias      MyJ/psi       J/psi
ChargeConj MyJ/psi       MyJ/psi
Alias      MyXi-         Xi-
Alias      MyKS          K_S0
ChargeConj MyKS          MyKS
Alias      Myeta         eta
ChargeConj Myeta         Myeta
#
Decay B0sig
  1.000    MyJ/psi  MyKS Myeta           PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyJ/psi
  1.000     mu+  mu-                    PHOTOS  VLL;
Enddecay
#
Decay MyKS
  1.000   pi+          pi-                      PHSP;
Enddecay
#
Decay Myeta
  1.0  gamma gamma   PHSP;
Enddecay 
#
End
#
