# EventType: 27103020
#
# Descriptor: [D*_s+ -> K+ K- pi+]cc
#
# NickName: Dsst_KKpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#    Weak decays of Ds* to KKpi final state.
#    Daughters in LHCb.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '^[D*_s+ => ^K- ^K+ ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )               ' ,
#     'fastTrack    =  ( GPT > 200 * MeV ) & ( GP  > 1.9 * GeV )         ' , 
#     'goodTrack    =  inAcc &  fastTrack                                ' ,     
#     'goodDs       =  ( GPT > 0.9 * GeV )         ' ,
#     'Bancestors   =  GNINTREE ( GBEAUTY , HepMC.ancestors )            ' , 
#     'notFromB     =  0 == Bancestors                                   ' 
# ]
#
# tightCut.Cuts     =    {
#     '[D*_s+]cc': 'goodDs & notFromB ' ,
#     '[K+]cc'  : 'goodTrack ' , 
#     '[pi+]cc' : 'goodTrack '
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min 
# Responsible: Marco Pappagallo
# Email: marco.pappagallo@cern.ch
# Date: 20170205
#
Decay D_s*+sig
  1.000        K+        K-        pi+             PHSP;
Enddecay
CDecay D_s*-sig
#
End
