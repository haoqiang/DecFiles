# EventType: 11134263
#
# Descriptor: [B0 -> K+ pi- (h_c -> (eta_c -> p+ anti-p-) gamma)]cc
#
# NickName: Bd_hcKpi,pp=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^([ B0 => ^K+ ^ pi- ^( h_c(1P) => ^gamma ^( eta_c(1S) => ^p+ ^p~- ) ) ]CC)'
# tightCut.Cuts      =    {
#     '[B0]cc'   : ' goodB  ' , 
#     '[p+]cc'   : ' goodProton  ' , 
#     '[K+]cc'    : ' goodKaon  ' , 
#     '[pi+]cc'    : ' goodPion  ' , 
#     'h_c(1P)'    : ' GALL ' ,
#     'eta_c(1S)'    : ' GALL ' ,
#     'gamma'     : ' goodPhoton ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns',
#     'from GaudiKernel.PhysicalConstants import c_light',
#     'inAcc = in_range( 0.005, GTHETA, 0.400)',
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'goodProton  = ( GPT > 250  * MeV ) & inAcc' , 
#     'goodKaon  = ( GPT > 150  * MeV )  & inAcc' , 
#     'goodPion  = ( GPT > 150  * MeV )  & inAcc' , 
#     'goodPhoton  = ( GPT > 150  * MeV ) & inAcc & inEcalX & inEcalY ' ,
#     'goodB  = ( GCTAU > 0.1e-3 * ns * c_light)' ]
#
#EndInsertPythonCode
#
# Documentation: B0 forced into hc K pi reconstructed in ppbar gamma K pi
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Lucio Anderlini (Firenze)
# Email:  lucio.anderlini@cern.ch
# Date: 20181214
#
## CPUTime:     < 1 min
#


Alias      Myhc        h_c
ChargeConj Myhc        Myhc

Alias      MyEtac       eta_c
ChargeConj MyEtac       MyEtac


#
Decay B0sig
  1.000    Myhc          K+    pi-             PHSP;
Enddecay
CDecay anti-B0sig
#
Decay Myhc
 1.000     MyEtac  gamma  VSP_PWAVE;
Enddecay
#
Decay MyEtac
  1.000     p+  anti-p-   PHSP;
Enddecay
#
End
#


