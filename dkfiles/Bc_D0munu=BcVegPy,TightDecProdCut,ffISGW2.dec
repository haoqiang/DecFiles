# EventType: 14573033
#
# Descriptor: [B_c+ -> (D0 -> K- pi+) mu+ nu_mu]cc
#
# NickName: Bc_D0munu=BcVegPy,TightDecProdCut,ffISGW2
#
# Production: BcVegPy
#
# Cuts: BcDaughtersInLHCb
# FullEventCuts: LoKi::FullGenEventCut/TightCuts
#
# Documentation: Bc decay to D0, mu+ and nu_mu with ISGW2 model. Daughters in acceptance. Momentum cuts 95% those from the stripping.
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool( LoKi__FullGenEventCut, "TightCuts" )
# tightCuts = Generation().TightCuts
# tightCuts.Code = "( count ( hasGoodB ) > 0 )"
#
# tightCuts.Preambulo += [
#       "from GaudiKernel.SystemOfUnits import GeV"
#     , "hasGoodMu         = GINTREE(( 'mu+' == GABSID ) & ( GPT > 0.95*GeV  ) & ( GP > 5.70*GeV ))"
#     , "hasGoodD0         = GINTREE(( 'D0'  == GABSID ) & ( GNINTREE(( 'K-' == GABSID ) & ( GPT > 0.23*GeV ) & ( GP > 1.9*GeV )) == 1 ) & ( GNINTREE(( 'pi+' == GABSID ) & ( GPT > 0.23*GeV ) & ( GP > 1.9*GeV )) == 1 ))"
#     , "hasGoodB           = ( GBEAUTY & GCHARM & hasGoodD0 & hasGoodMu )"
#      ]
# EndInsertPythonCode
#
# PhysicsWG: B2SL 
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Alison Tully
# Email: alison.tully@cern.ch
# Date: 20210204
#
Alias      MyD0        D0
Alias Myanti-D0   anti-D0
ChargeConj        MyD0       Myanti-D0
#
Decay B_c+sig
  1.000         MyD0   mu+   nu_mu          PHOTOS  ISGW2;
Enddecay
CDecay B_c-sig
#
Decay Myanti-D0
  1.000        K+        pi-                    PHSP;
Enddecay
CDecay MyD0
#
End
