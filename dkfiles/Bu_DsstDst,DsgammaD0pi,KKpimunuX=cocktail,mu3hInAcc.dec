# EventType: 12875613
# Descriptor: [B+ -> (D_s*+ -> (D_s+ => K- K+ pi+) gamma) (D~0* -> (D~0 -> K- mu+ nu_mu ) pi0 )]cc
# NickName: Bu_DsstDst,DsgammaD0pi,KKpimunuX=cocktail,mu3hInAcc
# Cuts: BeautyTo2CharmTomu3h
# CPUTime: < 1 min
# Documentation: Force B+ to combination of DsD0bar, Ds*D0bar, DsD*0bar and Ds*D*0bar. 
#                Then force all D0 to decay semileptonically, and force all Ds to decay to KKpi.
#                For background study of semileptonic Bs->(Ds->KKpi)MuNu decay.
#                Requires that the mu from charm and the 3h from other charm are in acc.
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Stephen Ogilvy
# Email: stephen.ogilvy@cern.ch
# Date: 20170920
#
Alias      MyD_s+     D_s+
Alias      MyD_s-     D_s-
ChargeConj MyD_s+     MyD_s-
Alias      MyD_s*+     D_s*+
Alias      MyD_s*-     D_s*-
ChargeConj MyD_s*+     MyD_s*-
Alias      MyD0       D0
Alias      Myanti-D0  anti-D0
ChargeConj MyD0       Myanti-D0
Alias      MyD*0       D*0
Alias      Myanti-D*0  anti-D*0
ChargeConj MyD*0       Myanti-D*0
#
Decay B+sig
  0.0100 Myanti-D0    MyD_s+                                   PHSP;
  0.0081 Myanti-D*0   MyD_s+                                   SVS;
  0.0076 MyD_s*+      Myanti-D0                                SVS;
  0.0171 MyD_s*+      Myanti-D*0                               SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
Enddecay
CDecay B-sig
#
Decay MyD_s+
  0.055     K+    K-     pi+          PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
  0.93500 MyD_s+    gamma                      VSP_PWAVE;
  0.05800 MyD_s+    pi0                        VSS;
Enddecay
CDecay MyD_s*-
#
Decay MyD0
  0.019200000 K*-     mu+     nu_mu                           PHOTOS  ISGW2;
  0.033300000 K-      mu+     nu_mu                           PHOTOS  ISGW2;
  0.000815539 K_1-    mu+     nu_mu                           PHOTOS  ISGW2;
  0.001374504 K_2*-   mu+     nu_mu                           PHOTOS  ISGW2;
  0.002370000 pi-     mu+     nu_mu                           PHOTOS  ISGW2;
  0.002015940 rho-    mu+     nu_mu                           PHOTOS  ISGW2;
  0.001007970 anti-K0 pi-     mu+     nu_mu                   PHOTOS   PHSP;
  0.000549802 K-      pi0     mu+     nu_mu                   PHOTOS   PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyD*0
  0.6470 MyD0      pi0                          VSS;
  0.3530 MyD0      gamma                        VSP_PWAVE;
Enddecay
CDecay Myanti-D*0
#
End

