# EventType: 13144221
#
# Descriptor: [B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> pi+ pi- gamma)]cc
#
# NickName: Bs_Jpsieta,mm,pipig=TightCut,LowPt
#
# Documentation: Low PT and acceptance cuts on charged particles, photons and Jpsi
# EndDocumentation
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = ' Beauty  => ^( J/psi(1S) => ^mu+ ^mu-)  ( eta => ^pi+ ^pi- ^gamma ) '
# tightCut.Cuts      =    {
#     'gamma'     : ' goodGamma ' ,
#     '[pi+]cc'   : ' goodPion  ' , 
#     '[mu+]cc'   : ' goodMuon  ' ,
#     'J/psi(1S)' : ' goodPsi   ' }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' , 
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' , 
#     'goodMuon  = ( GPT > 150  * MeV ) & inAcc   ' ,
#     'goodPion  = ( GPT > 150  * MeV ) & inAcc   ' ,
#     'goodGamma = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY ' ,
#     'goodPsi   = ( GPT > 500  * MeV ) & in_range ( 1.8 , GY , 4.5 )    ' ]
#
# EndInsertPythonCode
#
# PhysicsWG: B2Ch
# Tested: Yes
# Responsible: Max Chefdeville
# Email: chefdevi@lapp.in2p3.fr
# Date: 20220512
#
# CPUTime: < 1 min
#
Alias       MyJ/psi  J/psi
Alias       MyEta    eta
ChargeConj  MyJ/psi  MyJ/psi
ChargeConj  MyEta    MyEta
#
Decay B_s0sig
  1.000         MyJ/psi     MyEta        SVS;
Enddecay
CDecay anti-B_s0sig
#
Decay MyJ/psi
  1.000         mu+         mu-          PHOTOS VLL;
Enddecay
#
Decay  MyEta
  1.000     pi+ pi-  gamma                     PHOTOS PHSP;
Enddecay
End

