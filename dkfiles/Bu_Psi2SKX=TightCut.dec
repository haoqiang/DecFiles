# EventType: 12445000
#
# Descriptor: [B+ -> (psi(2S) -> mu+ mu-) (K*(1410)+ -> (K*(892)0 -> K+ pi-) pi+)]cc
#
# NickName: Bu_Psi2SKX=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: B+ -> psi(2S) K+ X, psi(2S) -> mu+ mu-.
#     Decays include several intermediate K*+, but exclude
#     B+ -> psi(2S) K+ pi0 final states.
#     Relative proportion of K* resonance components is
#     taken from Belle paper arXiv:1306.4894v3 and arXiv:1306.4894.
#     Included intermediate state:
#     K*(892)+, K*(1410)+, K2*(1430)+, K*(1680)+, K_1(1270)+ resonances
#     also non-resonant mode and decay with eta and omega.
#     The decay modes are taken from PDG.
#     Fractions of the decays are taken as a branching fraction, but then
#     scaled to make sum of fractions of all the final states to be 1.
#
#     Tight generator level cuts applied for muons and charged kaon in the
#     final state, which increases the statistics with the factor of 1.5:
#     Efficiency w/  tight cuts    in output           (20.28 +- 0.57)%
#                                  in GeneratorLog.xml ( 9.68 +- 0.42)%
#     Efficiency w/o tight cuts    in output           (26.66 +- 0.72)%
#                                  in GeneratorLog.xml (13.08 +- 0.55)%
# EndDocumentation
#
# Sample: SignalRepeatedHadronization
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalRepeatedHadronization 
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut   = signal.TightCut
# tightCut.Decay = '^[B+ --> (psi(2S) => mu+ mu-) K+ ...]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV                      ',
#     'inAcc        = in_range ( 0.005 , GTHETA , 0.400 )                                          ',
#     'inEta        = in_range ( 1.95  , GETA   , 5.050 )                                          ',
#     'inY          = in_range ( 1.9   , GY     , 4.6   )                                          ',
#     'fastTrack    = ( GPT > 180 * MeV ) & in_range( 2.9 * GeV, GP, 210 * GeV )                   ',
#     'goodK        = inAcc & inEta & fastTrack                                                    ',
#     'goodMu       = inAcc & inEta & fastTrack & (GPT > 500 * MeV)                                ',
#     'longLived    = 75 * micrometer < GTIME                                                      ',
#     'goodPsi      = inY & GINTREE( ("mu+" == GID) & goodMu ) & GINTREE( ("mu-" == GID) & goodMu )',
#     'goodBDaugPsi = GINTREE ( ("psi(2S)" == GABSID) & goodPsi )                                  ',
#     'goodBDaugK   = GINTREE ( ("K+"      == GABSID) & goodK   )                                  ',
#     'goodB        = inY & longLived & goodBDaugPsi & goodBDaugK                                  ',
# ]
# tightCut.Cuts = {
#     '[B+]cc': 'goodB',
# }
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Slava Matiunin
# Email: Viacheslav.Matiunin@<no-spam>cern.ch
# Date: 20211105
# CPUTime: <1 min
#
Define PKHplus 0.159
Define PKHzero 0.775
Define PKHminus 0.612
Define PKphHplus 1.563
Define PKphHzero 0.000
Define PKphHminus 2.712
#
Alias      Mypsi(2S) psi(2S)
ChargeConj Mypsi(2S) Mypsi(2S)
#
## K*(892)+
Alias      My2K*+ K*+
Alias      My2K*- K*-
ChargeConj My2K*+ My2K*-
#
## K*(892)0
Alias      My2K*0      K*0
Alias      My2anti-K*0 anti-K*0
ChargeConj My2K*0      My2anti-K*0
#
## K*(1410)+
Alias      My3K*+ K'*+
Alias      My3K*- K'*-
ChargeConj My3K*+ My3K*-
#
## K2*(1430)+
Alias      My4K*+ K_2*+
Alias      My4K*- K_2*-
ChargeConj My4K*+ My4K*-
#
## K*(1680)+
Alias      My5K*+ K''*+
Alias      My5K*- K''*-
ChargeConj My5K*+ My5K*-
#
## K0*(1430)+
Alias      My6K*+ K_0*+
Alias      My6K*- K_0*-
ChargeConj My6K*+ My6K*-
#
## K0*(1430)0
Alias      My6K*0      K_0*0
Alias      My6anti-K*0 anti-K_0*0
ChargeConj My6K*0      My6anti-K*0
#
## K_1(1270)+
Alias      MyK_1+ K_1+
Alias      MyK_1- K_1-
ChargeConj MyK_1- MyK_1+
#
Decay B+sig
    0.0148  Mypsi(2S) K+     pi-   pi+  PHSP ;
    0.0148  Mypsi(2S) K+     pi0   pi0  PHSP ;
    0.4308  Mypsi(2S) My3K*+            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.0289  Mypsi(2S) My4K*+            PHSP ;
    0.0365  Mypsi(2S) My5K*+            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.2173  Mypsi(2S) MyK_1+            SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
    0.1694  Mypsi(2S) K+     omega      PHSP ;
    0.0656  Mypsi(2S) K+     eta        PHSP ;
    0.0219  Mypsi(2S) My2K*+ eta        PHSP ;
Enddecay
CDecay B-sig
#
Decay Mypsi(2S)
    1.0000  mu+       mu-         PHOTOS VLL ;
Enddecay
#
## K*(892)+
Decay My2K*+
    1.0000  K+        pi0         VSS ;
Enddecay
CDecay My2K*-
#
## K*(892)0
Decay My2K*0
    1.0000  K+        pi-         VSS ;
Enddecay
CDecay My2anti-K*0
#
## K*(1410)+
Decay My3K*+
    0.0461  rho0      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.1908  My2K*+    pi0         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.7631  My2K*0    pi+         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
CDecay My3K*-
#
## K2*(1430)+
Decay My4K*+
    0.1075  rho0      K+          TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.1075  omega     K+          TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.1019  My2K*+    pi0         TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.4071  My2K*0    pi+         TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
    0.0552  My2K*+    pi+    pi-  PHSP ;
    0.2208  My2K*0    pi+    pi0  PHSP ;
Enddecay
CDecay My4K*-
#
## K*(1680)+
Decay My5K*+
    0.3865  rho0      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.1227  My2K*+    pi0         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.4908  My2K*0    pi+         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
CDecay My5K*-
#
## K_1(1270)+
Decay MyK_1+
    0.2799  rho0      K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.2199  omega     K+          VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.0355  My2K*+    pi0         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.1421  My2K*0    pi+         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0 ;
    0.0739  My6K*+    pi0         VSS;
    0.2487  My6K*0    pi+         VSS;
Enddecay
CDecay MyK_1-
#
## K0*(1430)+
Decay My6K*+
    0.7828  K+       pi0          PHSP ;
    0.2172  eta      K+           PHSP ;
Enddecay
CDecay My6K*-
#
## K0*(1430)0
Decay My6K*0
    1.0000  K+       pi-          PHSP ;
Enddecay
CDecay My6anti-K*0
#
End
#
