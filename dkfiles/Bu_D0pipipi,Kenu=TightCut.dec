# EventType: 12185008
#
# Descriptor: [B+ -> (anti-D0 -> K+ e- nu_e~) pi+ pi+ pi-]cc
# NickName: Bu_D0pipipi,Kenu=TightCut
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: This is the decay file for the B+ -> D0 bar ( -> K+ e- nu_e) pi+ pi- pi+ Phase space decay. The visible mass is required to be larger than 4500 MeV and the visible daughters as required to be inside the LHCb acceptance
# EndDocumentation
#
# InsertPythonCode:
#from Configurables import LoKi__GenCutTool
#from GaudiKernel.SystemOfUnits import MeV
#Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
#tightCut  = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay    = "[^(B+ => ^(D~0 => ^K+ ^e- ^nu_e~) ^pi+ ^pi+ ^pi-)]CC"
#tightCut.Cuts     = {
#   '[B+]cc'  : "(GNINTREE((GABSID == 'pi+') & (ACC)) == 3) & GINTREE((GABSID == 'e-') & (ACC)) & GINTREE((GABSID == 'K+') & (ACC)) & (BM2 > 20250000.)",
#   }
#tightCut.Preambulo += [
#   "BPX2 = (GCHILD(GCHILD(GPX, 1), 1) + GCHILD(GCHILD(GPX, 2), 1) + GCHILD(GPX, 2) + GCHILD(GPX, 3) + GCHILD(GPX, 4))**2",
#   "BPY2 = (GCHILD(GCHILD(GPY, 1), 1) + GCHILD(GCHILD(GPY, 2), 1) + GCHILD(GPY, 2) + GCHILD(GPY, 3) + GCHILD(GPY, 4))**2",
#   "BPZ2 = (GCHILD(GCHILD(GPZ, 1), 1) + GCHILD(GCHILD(GPZ, 2), 1) + GCHILD(GPZ, 2) + GCHILD(GPZ, 3) + GCHILD(GPZ, 4))**2",
#   "BPE2 = (GCHILD(GCHILD(GE, 1), 1) + GCHILD(GCHILD(GE, 2), 1) + GCHILD(GE, 2) + GCHILD(GE, 3) + GCHILD(GE, 4))**2",
#   "BM2  = (BPE2 - BPX2 - BPY2 - BPZ2)",
#   "ACC  = in_range(0.0075, GTHETA, 0.400)",
#   ]
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Tobias Tekampe
# Email: ttekampe@cern.ch
# Date: 20180119
# CPUTime: < 1min
#
Alias      MyD0        D0
Alias Myanti-D0   anti-D0
ChargeConj MyD0 Myanti-D0
#
Decay B+sig
  1.000     Myanti-D0  pi-  pi+ pi+              PHSP;
Enddecay
CDecay B-sig
#
Decay MyD0
  1.000     K-  e+ nu_e                          PHOTOS PHSP;
Enddecay
CDecay Myanti-D0
#
End
