# EventType: 15104571
#
# Descriptor: [Beauty -> p+ pi- (KS0 -> pi+ pi-) (pi0 -> gamma gamma)]cc
#
# NickName: Lb_Ksppi-pi0=TightCut,mKshhCut,PHSP 
#
# Cuts: LoKi::GenCutTool/TightCut 
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = '[^(Beauty => p+ pi- KS0 pi0)]CC'
# evtgendecay.mKshhCut.Cuts  = {'[Lambda_b0]cc' : ' mKshhCut '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = ( GMASS(CS('[(Beauty => ^p+ pi- KS0 pi0)]CC'),CS('[(Beauty => p+ ^pi- KS0 pi0)]CC'), CS('[(Beauty => p+ pi- ^KS0 pi0)]CC')) < 2.8 * GeV)"]
#
#
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[^(Beauty => ^p+ ^pi- (KS0 => ^pi+ ^pi-) pi0 )]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'        : ' inAcc' , 
#     '[p+]cc'         : ' inAcc' , 
#     '[Lambda_b0]cc'  : ' gammaPTCut'}
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "gammaPTCut   = ( ( (GCHILD(GPT,CS('[(Beauty => p+ pi- KS0 (pi0 => ^gamma gamma) )]CC')) > 1.5 * GeV) & in_range ( 0.005 , GCHILD(GTHETA,CS('[(Beauty => p+ pi- KS0 (pi0 => ^gamma gamma) )]CC')) , 0.400 ) )  | ( (GCHILD(GPT,CS('[(Beauty => p+ pi- KS0 (pi0 => gamma ^gamma) )]CC')) > 1.5 * GeV) & in_range ( 0.005 , GCHILD(GTHETA,CS('[(Beauty => p+ pi- KS0 (pi0 => gamma ^gamma) )]CC')) , 0.400 ) )  )",
#     "inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) "]
#
# EndInsertPythonCode
#
# Documentation: Bkgd for Kspipigamma, all in PHSP, pi pi in acceptance, at least one high PT photon in Acc
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20190106
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Alias      Mypi0        pi0
ChargeConj Mypi0        Mypi0
#
Decay Lambda_b0sig
  1.000   p+  pi-    MyK0s      Mypi0         PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyK0s
  1.000   pi+         pi-       PHSP;
Enddecay
#
Decay Mypi0
  1.000        gamma      gamma           PHSP;
Enddecay
#
End
