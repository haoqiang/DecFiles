# EventType: 11134264
#
# Descriptor: [B0 -> K+ pi- (chi_c1 -> (J/psi -> p+ anti-p-) gamma)]cc
#
# NickName: Bd_chic1Kpi,pp=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^([ B0 => ^K+ ^pi- ^( chi_c1(1P) => ^gamma ^( J/psi(1S) => ^p+ ^p~- ) ) ]CC)'
# tightCut.Cuts      =    {
#     '[B0]cc'   : ' goodB  ' , 
#     'chi_c1(1P)' : ' GALL ' ,
#     'J/psi(1S)' : ' GALL ' ,
#     '[p+]cc'   : ' goodProton  ' , 
#     '[pi+]cc'   : ' goodPion  ' , 
#     '[K+]cc'    : ' goodKaon  ' , 
#     'gamma'     : ' goodPhoton ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns',
#     'from GaudiKernel.PhysicalConstants import c_light',
#     'inAcc = in_range( 0.005, GTHETA, 0.400)',
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'goodProton  = ( GPT > 250  * MeV ) & inAcc' , 
#     'goodKaon  = ( GPT > 150  * MeV )  & inAcc' , 
#     'goodPion  = ( GPT > 150  * MeV )  & inAcc' , 
#     'goodPhoton  = ( GPT > 150  * MeV ) & inAcc & inEcalX & inEcalY ' ,
#     'goodB  = ( GCTAU > 0.1e-3 * ns * c_light)' ]
#
#EndInsertPythonCode
#
# Documentation: B0 forced into chic K pi reconstructed in ppbar gamma K pi
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Lucio Anderlini (Firenze)
# Email:  lucio.anderlini@cern.ch
# Date: 20181214
#
## CPUTime:     < 1 min
#


Alias      MyChic        chi_c1
ChargeConj MyChic        MyChic

Alias      MyJ/psi       J/psi
ChargeConj MyJ/psi       MyJ/psi


#
Decay B0sig
  1.000    MyChic          K+   pi-          PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyChic
 1.000     MyJ/psi  gamma  VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
#
Decay MyJ/psi
  1.000     p+  anti-p-   VLL;
Enddecay
#
End
#

