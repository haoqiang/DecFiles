# EventType: 13166032
# 
# Descriptor: [B_s0 -> (D_s- -> K- K+ pi-) (phi -> K- K+) pi+]cc
# 
# NickName: Bs_DsPhipi,KKpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool , 'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[Beauty ==> (D_s- ==> ^K- ^K+ ^pi-) (phi(1020) ==> ^K+ ^K-) ^pi+]CC"
# tightCut.Preambulo += [ "from LoKiCore.functions import in_range",
#                           "from GaudiKernel.SystemOfUnits import  GeV, mrad",
#                         ]
#tightCut.Cuts      =    {
# '[pi+]cc'   : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV )",
# '[K-]cc'   : "  in_range( 0.010 , GTHETA , 0.400 )  & ( GPT > 250 * MeV )"
#    }
# EndInsertPythonCode
#
# Documentation: Bs -> Ds phi pi with Ds -> K K pi, with tight cuts. Includes resonances in Ds decay
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# CPUTime: <1min
# Responsible: Matt Rudolph
# Email: matthew.scott.rudolph@cern.ch
# Date: 20221010
#
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      MyPhi      phi
ChargeConj MyPhi      MyPhi
#
#
Decay B_s0sig
  1.000    MyD_s-     MyPhi    pi+    PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s-
  1.000    K-         K+       pi-    D_DALITZ;
Enddecay
CDecay MyD_s+
#
Decay MyPhi
  1.000    K+         K-              VSS;
Enddecay
End
