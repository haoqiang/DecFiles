# EventType: 23103006
#
# Descriptor: [D_s+ => K+ K- pi+]cc
#
# NickName: Ds_KKpi=res,NotFromB
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
# Forces a Ds+ not from b quarks to K+ K- pi+ with generator level cuts;
# includes resonances in Ds decay;
# selections modeled on BeautyTomuCharmTo3h but without the mu cuts;
# see 23103005.
# EndDocumentation
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# signal = Generation().SignalPlain
# signal.addTool (LoKi__GenCutTool, 'TightCut')
#
# tightCut = signal.TightCut
# tightCut.Decay = '[D_s+ => ^K+ ^K- ^pi+]CC'
# tightCut.Preambulo += [
#     'inAcc = in_range(0.010, GTHETA, 0.400)',
#     'highPT = (GPT > 0.2375 * GeV)',
#     'highP = (GP > 1.9 * GeV)',
#     'notFromB = (0 == GNINTREE(GBEAUTY, HepMC.ancestors))',
#     'goodHadron = inAcc & highPT & highP',
#     'goodD = notFromB',
# ]
# tightCut.Cuts = {
#     '[K+]cc': 'goodHadron',
#     '[pi+]cc': 'goodHadron',
#     '[D_s+]cc': 'goodD',
# }
#
# EndInsertPythonCode
#
# CPUTime: < 1 min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Michael Wilkinson
# Email: miwilkin@syr.edu
# Date: 20201207
#
Decay  D_s+sig
  0.0539 K+ K- pi+ PHOTOS D_DALITZ;
Enddecay
CDecay D_s-sig
#
End
