# 
#
# EventType: 12875004
#
# Descriptor: [B- -> (D*_2(2460)0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi-) anti-nu_mu mu-]cc
#
# NickName: Bu_Dststmunu,Dstpi=CocktailHigher,TightCut
# Cuts: LoKi::GenCutTool/TightCut
# InsertPythonCode:

#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "[ (Beauty) ==> ^(D~0 -> ^K+ ^pi- {gamma} {gamma} {gamma}) ^mu+ nu_mu {X} {X} {X} {X} {X} {X} ]CC"
#tightCut.Preambulo += [
#  "from LoKiCore.functions import in_range"  ,
#  "from GaudiKernel.SystemOfUnits import GeV, MeV"  ,
#  "piKP     = GCHILD(GP,('K+' == GABSID )) + GCHILD(GP,('pi-' == GABSID ))" ,
#  "piKPT     = GCHILD(GPT,('K+' == GABSID )) + GCHILD(GPT,('pi-' == GABSID ))" ,
#]
#tightCut.Cuts      =    {
# '[pi+]cc'   : "( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GTHETA > 0.01 ) & ( GPT > 250 * MeV )" ,
# '[K-]cc'   : " ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GTHETA > 0.01 ) & ( GPT > 250 * MeV )" ,
# '[mu+]cc'  : " ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GTHETA > 0.01 ) & ( GP > 2950* MeV) ",
# '[D~0]cc'   : "( piKP > 15000 * MeV ) & (piKPT > 2450 * MeV)"
#    }
#
#from Configurables import LHCb__ParticlePropertySvc
#LHCb__ParticlePropertySvc().Particles = [
# "D*(2640)0           763      100423  0.0        2.6419   4.4175e-24                  D*(2S)0           0   0.5",
# "D*(2640)~0           764     -100423 0.0        2.6419   4.4175e-24                  anti-D*(2S)0           0   0.5",
# "D(2S)0              761      100421  0.0        2.518   3.3076e-24                   D(2S)0           0   0.5",
# "D(2S)~0              762     -100421  0.0        2.518   3.3076e-24                   anti-D(2S)0           0   0.5",
# "D*_2(2460)0         170         425  0.0        2.751   6.45306e-24                    D_2*0         425        0.5",
# "D*_2(2460)~0         166        -425 0.0        2.751   6.45306e-24                   anti-D_2*0        -425        0.5"
# ]
# EndInsertPythonCode

# Documentation: Sum of higher B -> D** mu nu modes . D** -> D*+ pi , D* -> D0 pi, D0 -> K pi. Cuts for B -> D* tau nu, tau-> mu Run2 analysis.
# EndDocumentation 
#
# CPUTime:< 1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Mark Smith 
# Email: mark.smith@cern.ch
# Date: 20210721
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD*(2S)0         D*(2S)0
Alias      MyAntiD*(2S)0         anti-D*(2S)0
ChargeConj MyD*(2S)0         MyAntiD*(2S)0
#
Alias      MyD(2S)0         D(2S)0
Alias      MyAntiD(2S)0         anti-D(2S)0
ChargeConj MyD(2S)0         MyAntiD(2S)0
#
#D(2460) used for D(2750), 2- state 
Alias      MyD(2750)0         D_2*0
Alias      MyAntiD(2750)0         anti-D_2*0
ChargeConj MyD(2750)0         MyAntiD(2750)0
#
#
Decay B-sig 
0.3  MyD*(2S)0      mu-  anti-nu_mu         PHOTOS  ISGW2;
0.3  MyD(2S)0       mu-  anti-nu_mu         PHOTOS  ISGW2;
0.3  MyD(2750)0     mu-  anti-nu_mu         PHOTOS  ISGW2;
Enddecay
CDecay B+sig
#
Decay MyD*(2S)0 
0.08     MyD*+ pi-                     PHOTOS PHSP;
Enddecay
CDecay MyAntiD*(2S)0
#
Decay MyD(2S)0 
0.08     MyD*+ pi-                     PHOTOS PHSP;
Enddecay
CDecay MyAntiD(2S)0
#
Decay MyD(2750)0 
0.08     MyD*+ pi-                     PHOTOS PHSP;
Enddecay
CDecay MyAntiD(2750)0
#
Decay MyD*+
1.0       MyD0   pi+                   VSS;
Enddecay
CDecay MyD*-
#
Decay MyD0
1.00   K-  pi+                           PHOTOS PHSP;
Enddecay
CDecay MyAntiD0
#
End
