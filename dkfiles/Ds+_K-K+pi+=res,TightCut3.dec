# EventType: 23103024
#
# Descriptor: [D_s+ -> K- K+ pi+]cc
#
# NickName: Ds+_K-K+pi+=res,TightCut3
#
# Cuts: 'LoKi::GenCutTool/TightCut'
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D_s+ => ^K- ^K+ ^pi+]CC'
# tightCut.Preambulo += [
#     "GVZ = LoKi.GenVertices.PositionZ() " ,
#     'from GaudiKernel.SystemOfUnits import micrometer, MeV' ,
#     'inAcc     = in_range ( 0.010, GTHETA, 0.380 ) ' , 
#     'inAccD     = in_range ( 0.020, GTHETA, 0.350 ) ' , 
#     'daughcuts = ( (GPT > 250 * MeV) & ( GP > 2000 * MeV))',
#     'Dcuts     = ( (GPT > 2500 * MeV) & ( GP > 14000 * MeV) & (GTIME > 60 * micrometer) )',
#     'Bancestors   =  GNINTREE ( GBEAUTY , HepMC.ancestors )                        ',
#     'notFromB  = ( 0 == Bancestors )  '
# ]
# tightCut.Cuts      =    {
#     '[K-]cc'  : ' inAcc & daughcuts',
#     '[pi+]cc'  : ' inAcc & daughcuts',
#     '[D_s+]cc'   : 'inAccD & Dcuts & notFromB'
#                         }
# EndInsertPythonCode
#
# Documentation: Decay products in acceptance, phase space decay model, P and PT cuts on D and daughters
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Erica Polycarpo 
# Email: Erica.Polycarpo@cern.ch 
# Date: 20200610
# CPUTime: < 1 min
#
Decay D_s+sig
  1.000        K-        K+        pi+          D_DALITZ;
Enddecay
CDecay D_s-sig
#
End
