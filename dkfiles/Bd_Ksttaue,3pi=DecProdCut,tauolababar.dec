# EventType: 11123000 
#
# Descriptor:  {[[B0]nos -> e+ (tau- -> pi+ pi- pi- nu_tau) (K*(892)0 -> K+ pi-)]cc, [[B0]nos -> (tau+ -> pi+ pi- pi+ anti-nu_tau) e- (K*(892)0 -> K+ pi-)]cc, [[B0]os -> e- (tau+ -> pi+ pi- pi+ anti-nu_tau) (K*(892)~0 -> K- pi+)]cc, [[B0]os -> (tau- -> pi+ pi- pi- nu_tau) e+ (K*(892)~0 -> K- pi+)]cc}
#
# NickName: Bd_Ksttaue,3pi=DecProdCut,tauolababar 
#
# Cuts: DaughtersInLHCb
#
#
# Documentation: Bd decay to K* tau e
# K* decays to Kpi final state.
# Tau lepton decay in the 3-prong charged pion mode using the Tauola BaBar model.
# All final-state products in the acceptance.
# Same as 11110010, but an electron replace the muon in the b decay
# EndDocumentation
#
# PhysicsWG: RD
#
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Francesco Polci, Pascal Vincent
# Email: francesco.polci@lpnhe.in2p3.fr, pascal.vincent@cern.ch
# Date: 20210320
#

# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias         Mytau+     tau+
Alias         Mytau-     tau-
ChargeConj    Mytau+     Mytau-
Alias         MyK*0      K*0
Alias         Myanti-K*0 anti-K*0
ChargeConj    MyK*0      Myanti-K*0
#
Decay B0sig
  0.500       MyK*0      Mytau+    e-         BTOSLLBALL 6;
  0.500       MyK*0      e+        Mytau-     BTOSLLBALL 6;
Enddecay
CDecay anti-B0sig
#
Decay MyK*0
  1.000       K+         pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Mytau-
  1.00        TAUOLA 5;
Enddecay
CDecay Mytau+
#
#Decay Mytau-
#  1.00        pi-        pi+      pi-    nu_tau     TAUSCALARNU;
#Enddecay
#CDecay Mytau+

#
End
