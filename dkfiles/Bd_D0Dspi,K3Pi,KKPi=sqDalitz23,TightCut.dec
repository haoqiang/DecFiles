# EventType: 11198045
# NickName: Bd_D0Dspi,K3Pi,KKPi=sqDalitz23,TightCut
# Descriptor: [B0 -> (D_s+ -> K+ K- pi+) (D~0 -> K+ pi+ pi- pi-) pi-]cc
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[B0 -> ^(D_s+ => ^K+ ^K- ^pi+) ^(D~0 ==> ^K+ ^pi+ ^pi- ^pi-) ^pi- ]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV)',
#    'goodD        = (GP > 8000 * MeV) & (GPT > 400 * MeV)',
#    'goodK        = in_range( 1.3 * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = in_range( 1.3 * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[B0]cc'   : 'goodB0',
#    '[D_s+]cc' : 'goodD',
#    '[D0]cc'   : 'goodD',
#    '[K+]cc'   : 'inAcc & goodK',
#    '[pi+]cc'  : 'inAcc & goodPi'
#    }
#EndInsertPythonCode
#
# Documentation:  B0 flat in Dalitz plot. D_s+ resonant decay forced
#    Decay file for B0 => D~0 D_s+ pi-
# EndDocumentation
# CPUTime: < 1 min
# 
# Date:   20201103
# Responsible: Ruiting Ma
# Email: ma.ruiting@cern.ch
# PhysicsWG: B2OC
# Tested: Yes

Alias My_D0    D0
Alias My_anti-D0    anti-D0
ChargeConj My_D0   My_anti-D0 

Alias My_D_s-    D_s-
Alias My_D_s+    D_s+
ChargeConj  My_D_s-    My_D_s+
#
Decay My_D0
  1.0  K-  pi+   pi+   pi-  LbAmpGen DtoKpipipi_v2;
Enddecay
CDecay My_anti-D0
#
Decay My_D_s+
  1.0   K+  K-   pi+   D_DALITZ;
Enddecay
CDecay My_D_s-
#
Decay B0sig
  1.000 My_D_s+ pi- My_anti-D0 FLATSQDALITZ;
Enddecay
CDecay anti-B0sig

End
