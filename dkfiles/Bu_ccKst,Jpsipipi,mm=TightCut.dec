# EventType: 12247130
#
# Descriptor: [B+ -> (K*+ -> (K_S0 -> pi+ pi-) pi+) (psi(2S) -> (J/psi -> mu+ mu-) pi+ pi-)]cc
#
# NickName: Bu_ccKst,Jpsipipi,mm=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: B+ decay to X(3872 | psi(2S)}[Psi pi+pi-] KS0 pi+ (90% K*(892), 10% phsp), tight cuts
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal     = generation.SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '^[B+ ==> ( (X_1(3872) | psi(2S) ) ==> ^( J/psi(1S) => ^mu+ ^mu- ) ^pi+ ^pi- ) ^pi+ ^( KS0 => pi+ pi-) ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import micrometer, millimeter, MeV, GeV ',
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'inY          =  in_range ( 1.9   , GY     , 4.6   )         ' ,
#     'inZ          =  GFAEVX(abs(GVZ), 0) < 2600. * millimeter    ' ,
#     'fastTrack    =  ( GPT > 180 * MeV ) & ( GP  > 3.0 * GeV )   ' ,
#     'fastKsTrack  =  ( GPT > 130 * MeV ) & ( GP  > 1.5 * GeV )   ' ,
#     'goodTrack    =  inAcc & inEta & fastTrack                   ' ,
#     'goodKsTrack  =  inAcc & inEta & fastKsTrack                 ' ,
#     'goodKsDaugPi =  GNINTREE (("pi+" == GABSID) & goodKsTrack, 4) > 1.5 ',
#     'goodKs       =  inZ & inAcc & inEta                         ' ,
#     'goodPsi      =  inY                                         ' ,
#     'longLived    =  75 * micrometer < GTIME                     ' ,
#     'goodB        =  inY & longLived                             ' 
# ]
# tightCut.Cuts     =    {
#     '[B+]cc'         : 'goodB     ' ,
#     'J/psi(1S)'      : 'goodPsi   ' ,
#     '[KS0]cc'        : 'goodKs & goodKsDaugPi ' ,
#     '[pi+]cc'        : 'goodTrack ' ,
#     '[mu+]cc'        : 'goodTrack & ( GPT > 500 * MeV ) '
#     }
#
# # Generator efficiency histos:
# tightCut.XAxis = ( "GPT/GeV" , 1.0 , 20.0 , 38  )
# tightCut.YAxis = ( "GY     " , 2.0 ,  4.5 , 10  )
#
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Pavel Krokovny
# Email: pavel.krokovny@cern.ch
# Date: 20200310
# CPUTime: < 1 min
#
Define PKHplus  0.159
Define PKHzero  0.775
Define PKHminus 0.612
Define PKphHplus  1.563
Define PKphHzero  0.0
Define PKphHminus 2.712
#
Alias      MyX_1(3872)  X_1(3872)
ChargeConj MyX_1(3872)  MyX_1(3872)
Alias      Mypsi2s      psi(2S)
ChargeConj Mypsi2s      Mypsi2s
Alias	   MyJ/psi      J/psi
ChargeConj MyJ/psi      MyJ/psi
Alias      MyK*+        K*+
Alias      MyK*-        K*-
ChargeConj MyK*+        MyK*-
Alias      MyKs         K_S0
ChargeConj MyKs         MyKs
Alias      Myrho0       rho0
ChargeConj Myrho0       Myrho0
#
Decay B+sig
   0.450     MyX_1(3872)  MyK*+         SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus;
   0.050     MyX_1(3872)  MyKs  pi+     PHOTOS PHSP;
   0.450     Mypsi2s      MyK*+         SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus;
   0.050     Mypsi2s      MyKs  pi+     PHOTOS PHSP;
Enddecay
CDecay B-sig
#
Decay MyX_1(3872)
   1.000     MyJ/psi        Myrho0      HELAMP 0.707107 0  0.707107 0  0.707107 0  0 0  -0.707107 0  -0.707107 0  -0.707107 0;
Enddecay
#
Decay Mypsi2s
   1.000     MyJ/psi   pi+   pi-        PHOTOS VVPIPI;
Enddecay
#
Decay MyJ/psi
   1.000     mu+       mu-              PHOTOS VLL;
Enddecay
#
Decay MyK*+
   1.000     MyKs  pi+                  PHOTOS VSS;
Enddecay
CDecay MyK*-
#
Decay MyKs
   1.000     pi+  pi-                   PHOTOS PHSP;
Enddecay
#
Decay Myrho0
   1.000      pi+  pi-                  PHOTOS VSS;
Enddecay
#
End
