# EventType: 27163472
#
# Descriptor: [D*+ -> (D0 -> pi- pi+ pi0) pi+]cc
#
# NickName: Dst_D0pi,pipipi0=TightCut,tighter,Dalitz
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#   This is the decay file for the decay D* -> D0 pi+
#   D0 decays to pi- pi+ pi0 with a D* tag.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Jolanta Brodzicka
# Email: Jolanta.Brodzicka@cern.ch
# Date: 20181012
# CPUTime: <1min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ D*(2010)+ => ^( D0 => ^pi+ ^pi- ^( pi0 -> ^gamma ^gamma ) ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV     ',
#     'inAcc       = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) )',
#     'goodD0Pi0   = ( GINTREE( ("gamma"==GABSID) & (GPT > 1200 * MeV) & inAcc & inCaloAcc ) )',
#     'goodD0Pip    = ( ("pi+"==GID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0Pim    = ( ("pi-"==GID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0      = ( (GPT > 1600 * MeV) & GINTREE(goodD0Pim) & GINTREE(goodD0Pip) & GINTREE(goodD0Pi0) )'
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'  : 'inAcc ',
#     '[D0]cc'   : 'goodD0 '
#     }
# EndInsertPythonCode

Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0

Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig

Decay MyD0
  1.000   pi+ pi- pi0  D_DALITZ;
Enddecay
CDecay MyantiD0
#
End
 
