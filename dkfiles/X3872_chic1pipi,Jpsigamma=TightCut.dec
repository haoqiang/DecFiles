# EventType: 28144252
#
# Descriptor: chi_c2 -> (chi_c1 -> (J/psi(1S) -> mu+ mu-) gamma) pi+ pi-
#
# ParticleValue: "chi_c2(1P) 765 445 0.0 3.87169 -3.17e-4 chi_c2 445 0.001"
#
# NickName: X3872_chic1pipi,Jpsigamma=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Decay of prompt X(3872) state into chi_c1(1P) pi+ pi- final state
# - X(3872) generated as chi_c2(1P) with modified mass/width to apply LoKi cuts (not possible with the Special generation tool);
# - generator level cuts to gain number of events on disk (converted photons will be required);
# - X3840_D0D0bar=TightCut.dec by Vanya Belyaev took as reference to improve CPU performance (only charmonium production is activated in Pythia8) 
# - same generator level cuts of 28144253 (run on same Jpsipipigamma final state)
# EndDocumentation
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
#
# generation = Generation()
# signal     = generation.SignalPlain
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut            = signal.TightCut
# tightCut.Decay      = 'Meson => (chi_c1(1P) => ^(J/psi(1S) => ^mu+ ^mu-) ^gamma) ^pi+ ^pi-'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV' ,
#     'inAcc          =  in_range ( 0.005 , GTHETA , 0.400 )                           ' ,
#     'inEta          =  in_range ( 1.95  , GETA   , 5.050 )                           ' ,
#     'inY            =  in_range ( 1.9 , GY , 4.6 )                                   ' ,
#     'lhcbTrack      =  inAcc & inEta                                                 ' ,
#     'recoTrack      =  ( GPT > 250 * MeV ) & ( GP > 2.6 * GeV )                      ' , 
#     'goodJpsi       =  inY & ( GPT > 2.0 * GeV )                                     ' ,
#     'goodMuon       =  lhcbTrack & recoTrack & ( GP > 5.0 * GeV )                    ' ,
#     'goodGamma      =  ( GPT > 350 * MeV ) & ( GP > 4.8 * GeV)                       ' ,
#     'goodPion       =  lhcbTrack & recoTrack & ( GP < 160.0 * GeV )                  ' ]
# tightCut.Cuts       =    {
#     'J/psi(1S)'     : 'goodJpsi'  ,
#     '[mu+]cc'       : 'goodMuon'  ,
#     'gamma'         : 'goodGamma' ,
#     '[pi+]cc'       : 'goodPion'  }
#
# # -- modify Pythia8 to only generate from Charmonium processes -- #
# from Configurables import Generation, MinimumBias, Pythia8Production, Inclusive, SignalPlain, SignalRepeatedHadronization, Special
#
# Pythia8TurnOffMinbias  = [ "SoftQCD:all     = off" ]
# Pythia8TurnOffMinbias += [ "Bottomonium:all = off" ]
# Pythia8TurnOffMinbias += [ "Charmonium:all  =  on" ]
#
# gen = Generation()
# gen.addTool( MinimumBias , name = "MinimumBias" )
# gen.MinimumBias.ProductionTool = "Pythia8Production"
# gen.MinimumBias.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.MinimumBias.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Inclusive , name = "Inclusive" )
# gen.Inclusive.ProductionTool = "Pythia8Production"
# gen.Inclusive.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Inclusive.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalPlain , name = "SignalPlain" )
# gen.SignalPlain.ProductionTool = "Pythia8Production"
# gen.SignalPlain.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalPlain.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalRepeatedHadronization , name = "SignalRepeatedHadronization" )
# gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
# gen.SignalRepeatedHadronization.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Special , name = "Special" )
# gen.Special.ProductionTool = "Pythia8Production"
# gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Special.Pythia8Production.Commands += Pythia8TurnOffMinbias
# # -- END  -- #
# EndInsertPythonCode
#
# CPUTime: < 1 min
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Giovanni Cavallero
# Email: giovanni.cavallero@cern.ch
# Date: 20200214
#

Alias   MyChic1  chi_c1
Alias   MyJpsi  J/psi
#
Decay chi_c2sig
  1.000     MyChic1     pi+     pi-     PHOTOS     PHSP;
Enddecay
#
Decay MyChic1
  1.000     gamma     MyJpsi     PHSP;
Enddecay
Decay MyJpsi
  1.000     mu+     mu-     PHOTOS     VLL;
Enddecay
#
End
#
