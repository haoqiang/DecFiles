# EventType: 12165720
#
# Descriptor: [B- -> (D*(2007)0 -> (D0 -> (KS0 -> pi+ pi-) pi+ pi- pi0) gamma) (rho(770)- -> pi- pi0)]cc
#
# NickName: Bu_Dst0Rho-,D0gamma,KSpipipi0=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B- => (D*(2007)0 => ^(D0 => ^(KS0 ==> ^pi+ ^pi-) ^pi+ ^pi- ^pi0) gamma) ^(rho(770)- => ^pi- pi0)]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'goodB        = (GP > 55000 * MeV) & (GPT > 5000 * MeV) & (GTIME > 0.135 * millimeter)',
#     'goodD        = (GP > 25000 * MeV) & (GPT > 1800 * MeV)',
#     'goodKS       = (GFAEVX(abs(GVZ), 0) < 2500.0 * millimeter)',
#     'goodDDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 1000 * MeV) & inAcc, 1) > 1.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 1) > 1.5)',
#     'goodBachPi   = (GNINTREE (("pi-" == GABSID) & (GP > 5000 * MeV) & (GPT > 500 * MeV) & inAcc, 1) > 0.5)',
#     'goodPi0      = (GPT > 400 * MeV) & inAcc'
# ]
# tightCut.Cuts      =    {
#     '[B-]cc'         : 'goodB',
#     '[rho(770)-]cc'  : 'goodBachPi',
#     '[D0]cc'         : 'goodD  & goodDDaugPi',
#     '[KS0]cc'        : 'goodKS & goodKsDaugPi',
#     '[pi-]cc'        : 'inAcc',
#     '[pi0]cc'        : 'goodPi0'
#     }
# EndInsertPythonCode
#
# Documentation: B to Dst0rho, Dst0 to D0gamma, D0 to Ks0pipipi0 (phase space), K0s forced into pi+ pi- and rho into pi+pi0, 
# D0 decay without resonance, decay products in acceptance and tight cuts
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: 3 min
# Responsible: Jessy Daniel
# Email: jessy.daniel@clermont.in2p3.fr
# Date: 20210630
#

Alias        MyD*0      D*0
Alias        Myanti-D*0 anti-D*0
ChargeConj   MyD*0      Myanti-D*0
Alias        MyRho-     rho-
Alias        MyRho+     rho+
ChargeConj   MyRho-     MyRho+
Alias      Myanti-D0   anti-D0
Alias      MyD0        D0
ChargeConj Myanti-D0        MyD0
Alias myK_S0      K_S0
ChargeConj myK_S0 myK_S0
##
Decay B-sig
  1.000     MyD*0   MyRho-      SVV_HELAMP 0.122 1.02 0.944 0.000 0.306 0.65;   #cf PHYSICAL REVIEW D 67, 112002 (2003)
Enddecay
CDecay B+sig
#
#
Decay MyRho-
  1.000   pi-   pi0              VSS;
Enddecay
Decay MyRho+
  1.000  pi+ pi0                 VSS ;
Enddecay
#
#
Decay MyD*0
1.000 MyD0  gamma         VSP_PWAVE;
Enddecay
CDecay Myanti-D*0
#
Decay MyD0
1.000     myK_S0 pi+  pi- pi0       PHSP;
Enddecay
CDecay Myanti-D0
#
Decay myK_S0
1.000     pi+  pi-               PHSP;
Enddecay
#
End

