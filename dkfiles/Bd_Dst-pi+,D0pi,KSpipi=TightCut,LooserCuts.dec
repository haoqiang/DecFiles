# EventType: 11166145
#
# Descriptor: [B0 -> (D*(2010)- -> (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) pi-) pi+]cc
#
# NickName: Bd_Dst-pi+,D0pi,KSpipi=TightCut,LooserCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[B0 => (D*(2010)- -> ^(D~0 => ^(KS0 => pi+ pi-) pi+ pi-) pi- ) ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'goodB        = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter)',
#     'goodD        = (GP > 10000 * MeV) & (GPT > 500 * MeV)',
#     'goodKS       = (GP > 4000 * MeV) & (GPT > 250 * MeV)',
#     'goodDDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 1000 * MeV) & inAcc, 1) > 1.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 1750 * MeV) & inAcc, 1) > 1.5)',
#     'goodBachPi    = (GNINTREE (("pi+"  == GABSID) & (GP > 4000 * MeV) & (GPT > 400 * MeV) & inAcc, 1) > 0.5)'
# ]
# tightCut.Cuts      =    {
#     '[B0]cc'         : 'goodB  & goodBachPi',
#     '[D0]cc'         : 'goodD  & goodDDaugPi',
#     '[KS0]cc'        : 'goodKS & goodKsDaugPi',
#     '[pi+]cc'        : 'inAcc'
#     }
#EndInsertPythonCode
#
# Documentation: D*+ forced to D0 pi+, D0 forced to KSpipi
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Seophine Stanislaus 
# Email: seophine.stanislaus@physics.ox.ac.uk
# Date: 20211015
#CPUTime: <1min
#
Alias         MyD*+       D*+
Alias         MyD*-       D*-
ChargeConj    MyD*+       MyD*-
Alias         MyD0        D0
Alias         Myanti-D0   anti-D0
ChargeConj    MyD0        Myanti-D0
Alias         MyK_S0      K_S0
ChargeConj    MyK_S0      MyK_S0
##
Decay B0sig
  1.000    MyD*-        pi+          SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyD*-
  1.000    Myanti-D0    pi-          VSS;
Enddecay
CDecay MyD*+
#
Decay Myanti-D0
  1.000     MyK_S0  pi+  pi-        PHSP;
Enddecay
CDecay MyD0
#
Decay MyK_S0
  1.000     pi+         pi-         PHSP;
Enddecay
#
End
