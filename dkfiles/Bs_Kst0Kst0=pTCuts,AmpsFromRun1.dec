# EventType: 13104007
#
# Descriptor: [B_s0 => (K*(892)0 => K+ pi-) (K*(892)~0 => K- pi+)]cc
#
# NickName: Bs_Kst0Kst0=pTCuts,AmpsFromRun1
#
# Cuts: 'LoKi::GenCutTool/TightCut'
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^( B_s0 -> (K*(892)0 -> ^K+ ^pi-) (K*(892)~0 -> ^K- ^pi+) )]CC'
#
# tightCut.Preambulo += [
#   "in_acc = in_range( 0.010 , GTHETA , 0.400 )",
#   "good_track = ( GPT > 300 * MeV ) & in_acc",
#   "good_Bs = ( ( GMINTREE(GPT,GID=='K+') + GMINTREE(GPT,GID=='K-') + GMINTREE(GPT,GID=='pi-') + GMINTREE(GPT,GID=='pi+') ) > 3000 * MeV )"
# ]
#
# tightCut.Cuts = {
#   '[pi+]cc' : 'good_track',
#   '[K-]cc'  : 'good_track',
#   '[B_s0]cc': 'good_Bs'
# }
# EndInsertPythonCode
#
# Documentation:
#
# B_s0 decaying into two vectors K*(892)0 and K*(892)~0.
# The sum of the pTs of the daughters is required to be greater than a minimum.
# K*(892)0 and K*(892)~0 decaying into (K+ pi-) and (K- pi+), respectively.
# Kaons and pions are required to be in acceptance and have a minimum pT.
# The transversity amplitudes are taken from https://arxiv.org/abs/1712.08683.
#
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Asier Pereiro
# Email: asier.pereiro.castro@cern.ch
# Date: 20221019
#
Define Azero  0.45607 # Mod. of Azero
Define pAzero 0.0     # Phase of Azero
Define Apar   0.54498 # Mod. of Apar
Define pApar  2.40    # Phase of Apar
Define Aperp  0.70356 # Mod. of Aperp
Define pAperp 2.62    # Phase of Aperp
Define beta_s 0.0     # Weak phase
Define eta    1.0     # B tag (+1 or -1). Set to 1 as is ignored in the model
#
Alias      MyK*0       K*0
Alias      Myanti-K*0  anti-K*0
ChargeConj MyK*0       Myanti-K*0  
#
Decay B_s0sig
  1.000    MyK*0    Myanti-K*0   PVV_CPLH beta_s eta Apar pApar Azero pAzero Aperp pAperp; 
Enddecay
CDecay anti-B_s0sig
#
Decay MyK*0
  1.000 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0
#
End
