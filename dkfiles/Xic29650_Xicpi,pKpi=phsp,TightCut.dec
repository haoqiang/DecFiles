# EventType: 26264086
#
# Descriptor: [Xi_c0 -> [Xi_c+ -> p+ K- pi+] pi- ]cc
#
# NickName: Xic29650_Xicpi,pKpi=phsp,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# CPUTime: 6 min
#
# Documentation: (prompt) Excited Xi_c0 decay according to phase space decay model with tight cuts.
#                 Mass = 2965 MeV and Width = 14.1 MeV
#                 Xi_c mimic Xi_c* or Xi_c**
# EndDocumentation
#
# ParticleValue: "Xi_c0               106        4132  0.0        2.96488   0.46680e-022                    Xi_c0        4132   0.0", "Xi_c~0              107       -4132  0.0        2.96488   0.4668e-022              anti-Xi_c0       -4132   0.0"
# 
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[Xi_c0 => ^(Xi_c+ ==> ^p+ ^K- ^pi+) ^pi-]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter,micrometer,MeV,GeV',
#     'GY           =  LoKi.GenParticles.Rapidity () ## to be sure ' , 
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'fastTrack    =  ( GPT > 300 * MeV ) & ( GP  > 3.0 * GeV )   ' , 
#     'goodTrack    =  inAcc & inEta                               ' ,     
#     'goodLc       =  ( GPT > 1.5 * GeV )   ' ,
# ]
# tightCut.Cuts     =    {
#     '[Xi_c+]cc'      : 'goodLc   ' ,
#     '[K+]cc'         : 'goodTrack & fastTrack' ,
#     '[pi+]cc'        : 'goodTrack & fastTrack' , 
#     '[p+]cc'         : 'goodTrack & fastTrack & ( GP > 9 * GeV ) '
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Zhihao Xu
# Email:       zhihao.xu@cern.ch
# Date: 20201006
#
Alias MyK*0         K*0
Alias Myanti-K*0    anti-K*0
ChargeConj MyK*0    Myanti-K*0
#
Alias MyXi_c+       Xi_c+
Alias Myanti-Xi_c-  anti-Xi_c-
ChargeConj MyXi_c+  Myanti-Xi_c-
#
Decay Xi_c0sig
  1.000         MyXi_c+   pi-  PHSP;
Enddecay
CDecay anti-Xi_c0sig
#
Decay MyXi_c+
  0.09400       p+      K-      pi+     PHSP;
  0.11600       p+      Myanti-K*0      PHSP;
Enddecay
CDecay Myanti-Xi_c-
#
Decay MyK*0
  1.000         K+      pi-     VSS;
Enddecay
CDecay Myanti-K*0
#
###### Overwrite forbidden decays
Decay Xi'_c0
1.0000    gamma     Sigma_c0                PHSP;
Enddecay
CDecay anti-Xi'_c0
#
Decay Xi_c*0
0.5000    Sigma_c0  pi0                     PHSP;
0.5000    Sigma_c0  gamma                   PHSP;
Enddecay
CDecay anti-Xi_c*0
#
End 
