# EventType: 11563003
#
# Descriptor: {[[B0]nos ->(D- -> K+ pi- pi-) (tau+ -> pi+ pi- pi+ pi0 anti-nu_tau) nu_tau]cc, [[B0]os -> (D+ -> K- pi+ pi+) (tau- -> pi- pi+ pi- pi0 nu_tau) anti-nu_tau]cc}
#
# NickName: Bd_D-taunu,Kpipi,3pipi0nu,tauola=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[( [B0]cc => ^(D+ => K- pi+ pi+) ^(tau- => pi- pi+ pi- pi0 nu_tau) nu_tau~ ) ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#     ,"CS  = LoKi.GenChild.Selector"
#     ,'goodDp = ( (GP>12000*MeV) & (GPT>1500*MeV) & ( (GCHILD(GPT,("K+" == GABSID )) > 1400*MeV) & (GCHILD(GP,("K+" == GABSID )) > 5000*MeV) & in_range ( 0.010 , GCHILD(GTHETA,("K+" == GABSID )) , 0.400 ) ) & ( (GCHILD(GPT,CS("[D+ => K- ^pi+ pi+ ]CC")) > 200*MeV) & (GCHILD(GP,CS("[D+ => K- ^pi+ pi+ ]CC")) > 1600*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[D+ => K- ^pi+ pi+ ]CC")) , 0.400 ) ) & ( (GCHILD(GPT,CS("[D+ => K- pi+ ^pi+ ]CC")) > 200*MeV) & (GCHILD(GP,CS("[D+ => K- pi+ ^pi+ ]CC")) > 1600*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[D+ => K- pi+ ^pi+ ]CC")) , 0.400 ) )  )'
#     ,"goodTau = ( ( (GCHILD(GPT,1) > 200*MeV) & (GCHILD(GP,1) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,1) , 0.400 ) ) & ( (GCHILD(GPT,2) > 200*MeV) & (GCHILD(GP,2) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,2) , 0.400 ) ) & ( (GCHILD(GPT,3) > 200*MeV) & (GCHILD(GP,3) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,3) , 0.400 ) ) )"
# ]
# tightCut.Cuts = {
#      '[D+]cc': 'goodDp'
#     ,'[tau-]cc': 'goodTau'
#     }
# EndInsertPythonCode
#
# Documentation: B0 to D- tau nu, with D- -> K pi pi and tau -> 3pi pi0 nu.
# Daughters in LHCb Acceptance and passing StrippingB0d2DTauNuForB2XTauNuAllLines cuts.
# Tau forced to 3pi pi0 nu final state using TAUOLA model.
# EndDocumentation
#
# PhysicsWG: B2SL
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Antonio Romero Vidal
# Email: antonio.romero@usc.es
# Date: 20200629
#
Alias         MyD+     D+
Alias         MyD-     D-
ChargeConj    MyD+     MyD-
#
Alias         MyTau-   tau-
Alias         MyTau+   tau+
ChargeConj    MyTau-   MyTau+
#
Decay B0sig
  1.00        MyD-     MyTau+    nu_tau     ISGW2;
Enddecay
CDecay anti-B0sig
#
Decay MyD-
  1.000       K+       pi-       pi-        D_DALITZ;
Enddecay
CDecay MyD+
#    
Decay MyTau-
  1.00	     TAUOLA 8;
Enddecay
CDecay MyTau+
#   
End
#
