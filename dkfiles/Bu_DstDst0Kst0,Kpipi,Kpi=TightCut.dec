# EventType: 12197401
#
# Descriptor: [B+ -> (D*+ -> (D+ -> K- pi+ pi+) pi0) (K*0 -> K+ pi-) (anti-D*0 -> (anti-D0 -> K+ pi-) pi0)]cc
#
# NickName: Bu_DstDst0Kst0,Kpipi,Kpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
##
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool , 'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B+ => (D*(2010)+ => (D+ => ^K- ^pi+ ^pi+) X0 )  (D*(2007)~0 => (D~0 => ^K+ ^pi-) X0) (K*(892)0 => ^K+ ^pi-)  ]CC"
# tightCut.Preambulo += [ "from LoKiCore.functions import in_range",
#                           "from GaudiKernel.SystemOfUnits import  GeV, mrad",
#                         ]
#tightCut.Cuts      =    {
# '[pi+]cc'   : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV )",
# '[K-]cc'   : "  in_range( 0.010 , GTHETA , 0.400 )  & ( GPT > 250 * MeV )"
#    }
# EndInsertPythonCode
#
# Documentation:Decay File For B+- ->D*+ D*0 K*0 with D+->Kpipi and D0->Kpi
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Matt Rudolph
# Email: matthew.scott.rudolph@cern.ch
# Date: 20190604
# CPUTime: <1min
Alias My_D*+ D*+
Alias My_D*- D*-
Alias My_D*0 D*0
Alias My_anti-D*0 anti-D*0
Alias My_D0 D0
Alias My_anti-D0 anti-D0
Alias My_D+ D+
Alias My_D- D-
Alias My_K*0    K*0
Alias My_anti-K*0    anti-K*0
ChargeConj My_D0 My_anti-D0
ChargeConj My_D+ My_D-
ChargeConj My_D*+ My_D*-
ChargeConj My_D*0 My_anti-D*0
ChargeConj My_K*0 My_anti-K*0

#D*+ Decay
Decay My_D*+
  0.307 My_D+ pi0  VSS;
  0.016 My_D+ gamma  VSP_PWAVE;
Enddecay
CDecay My_D*-

#D+ Decay
Decay My_D+
  1.0 K- pi+ pi+ D_DALITZ;
Enddecay
CDecay My_D-

Decay My_anti-D*0
  0.647   My_anti-D0 pi0 VSS;
  0.353   My_anti-D0 gamma VSP_PWAVE;
Enddecay
CDecay My_D*0

#D0 Decay
Decay My_D0
  1.0 K- pi+ PHSP;
Enddecay
CDecay My_anti-D0

Decay My_K*0
  1.0 K+ pi- VSS;
Enddecay
CDecay My_anti-K*0


Decay B+sig
  1.0 My_D*+ My_anti-D*0 My_K*0 PHSP;
Enddecay
CDecay B-sig
#
End
#
