# EventType:  27163470
# 
# Descriptor: [D*+ -> ( D0 -> (phi(1020) -> K+ K-) pi0 ) pi+]cc
#
# NickName: Dst_D0pi,KKpi0=TightCut,tighter
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Dst-> (D0 -> phi(-> K+ K- ) pi0 ) pi+
# D0 decaying into phi pi0
# phi decaying into K+ K-
# All final-state products in the acceptance.
# EndDocumentation 
#
# PhysicsWG: Charm
#
# Tested: Yes
# Responsible: Jolanta Brodzicka
# Email: Jolanta.Brodzicka@cern.ch
# Date: 20181012
# CPUTime: <1min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ D*(2010)+ => ^( D0 => (phi(1020) => ^K+ ^K-) ^( pi0 -> ^gamma ^gamma ) ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV     ',
#     'inAcc       = in_range ( 0.005 , GTHETA , 0.400 ) ',
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) )',
#     'goodD0Pi0   = ( GINTREE( ("gamma"==GABSID) & (GPT > 1200 * MeV) & inAcc & inCaloAcc ) )',
#     'goodD0Km    = ( ("K-"==GID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0Kp    = ( ("K+"==GID) & (GPT > 400 * MeV) & inAcc )',
#     'goodD0      = ( (GPT > 1600 * MeV) & GINTREE(goodD0Km) & GINTREE(goodD0Kp) & GINTREE(goodD0Pi0) )'
# ]
# tightCut.Cuts      =    {
#     '[pi+]cc'  : 'inAcc ',
#     '[D0]cc'   : 'goodD0 '
#     }
# EndInsertPythonCode

Alias myD0 D0
Alias myantiD0 anti-D0
ChargeConj myD0 myantiD0
Alias      Myphi   phi
ChargeConj Myphi   Myphi
#
Decay D*+sig
  1.000 myD0  pi+    VSS;
Enddecay
CDecay D*-sig

Decay myD0
  1.0  Myphi        pi0      SVS;
Enddecay
CDecay myantiD0

#
Decay Myphi
  1.000     K+    K-        VSS;
Enddecay
#
End

