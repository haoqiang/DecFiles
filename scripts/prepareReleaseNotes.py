#!/usr/bin/env python
'''
Code to read gitlab merge requests in DecFiles package and writes out
release notes. It works properly when new decay files are added but has
some issues in other cases (which do not happen too often, so it is not too
useful to spend time to handle them). 

Usage:
  ./scripts/prepareReleaseNotes.py previousTag newTag outputFile.md

Requires LCG view (or local environment with python gitlab module
available.
'''

# requires python3
from __future__ import print_function, unicode_literals

import sys, os
import datetime
from dateutil.parser import parse as tparser
import gitlab
import argparse as ap

def getTagDate(repo, tagName):
  '''Function to determine date when given tag was created. Returns
datetime object. Inputs are Gitlab project object (repo) and string with
tag name (tagName).'''
  tags = repo.tags.list(all=True)
  for tag in tags:
    if tag.attributes['name']==tagName:
      tt = tparser(tag.attributes['commit']['created_at'])
                                      
      return tt

def filterMRs(allMRs, date, target=None):
  '''Function which checks all merge requests and returns list with those
merged after given date/time.'''
  result = []
  for mr in allMRs:
    if mr.attributes['merged_at'] == None:
      continue
    if target != None and mr.attributes['target_branch'] != target:
      continue
    tmpDate = tparser(mr.attributes['merged_at'])
    if tmpDate > date:
      result.append(mr)
  return result 

def findEventType(decayFile, changeType='+'):
  '''Function to parse single change in the merge request and works out
event type and nickname. This function works OK only for new decay
files.'''
  eventType = 0
  nickname = ''
  for line in decayFile.split('\n'):
    if len(line)==0 or line[0] != changeType:
      continue
    if eventType==0 and 'EventType' in line:
      temp = line.split(':')
      eventType = int(temp[len(temp)-1])
    if 'NickName' in line:
      temp = line.split(':')
      nickname = temp[len(temp)-1].strip()
  return eventType, nickname

def eventTypeFromDkFile(fileName):
  eventType = 0
  nickname = ''
  if not os.path.exists(fileName):
    return eventType, nickname
  ff = open(fileName)
  for line in ff:
    if eventType==0 and 'EventType' in line:
      temp = line.split(':')
      eventType = int(temp[len(temp)-1])
    if 'NickName' in line:
      temp = line.split(':')
      nickname = temp[len(temp)-1].strip()
  ff.close()
  return eventType, nickname

def formatMR(mr, output):
  '''Function to format release notes for single merge request.'''
  mrId = mr.attributes['iid']
  mrAuthor = mr.attributes['author']['name']
  mrDate = str(tparser(mr.attributes['created_at']).date())
  changes = dict()
  newFiles = dict()
  rmFiles = dict()
  for change in mr.changes()['changes']:
    if '.dec' not in change['new_path']:
      continue
    if change['deleted_file']:
      eventType, nickname = findEventType(change['diff'], '-')
      rmFiles[eventType] = nickname
    else:
      eventType, nickname = findEventType(change['diff'])
      if eventType==0 or nickname=='':
        eventType, nickname = eventTypeFromDkFile(change['new_path'])
      if change['new_file']:
        newFiles[eventType] = nickname
      else:
        changes[eventType] = nickname

  output.write('! %s - %s (MR !%d)  \n' % (mrDate, mrAuthor, mrId))
  if len(newFiles.keys())>0:
    if len(newFiles.keys())==1:
      output.write('   Add new decay file  \n')
    else:
      output.write('   Add %d new decay files  \n' % len(newFiles.keys()))
    for key in newFiles.keys():
      output.write('   + %d : %s  \n' % (key, newFiles[key]))
  if len(changes.keys())>0:
    if len(changes.keys())==1:
      output.write('   Modify decay file  \n')
    else:
      output.write('   Modify %d decay files  \n' % len(changes.keys()))
    for key in changes.keys():
      output.write('   + %d : %s  \n' % (key, changes[key]))
  if len(rmFiles.keys())>0:
    if len(rmFiles.keys())==1:
      output.write('   Remove decay file  \n')
    else:
      output.write('   Remove %d decay files  \n' % len(rmFiles.keys()))
    for key in rmFiles.keys():
      output.write('   + %d : %s  \n' % (key, rmFiles[key]))

  if len(newFiles.keys())==0 and len(changes.keys())==0: 
    output.write('   %s  \n' % mr.attributes['description'])

  output.write('  \n')
  return

if __name__ == '__main__':
  # arguments should be last tag, new tag, outputfile
  parser = ap.ArgumentParser(description='Release notes')
  parser.add_argument('-lastTag', required=True)
  parser.add_argument('-newTag', required=True)
  parser.add_argument('-output', required=True)
  parser.add_argument('-branch', required=True)

  args = parser.parse_args()

  gl = gitlab.Gitlab('https://gitlab.cern.ch/')
  project = gl.projects.get(3464)
  oldTagDate = getTagDate(project, args.lastTag)
  
  allMRs = project.mergerequests.list(all=True,state='merged')
  relevantMRs = filterMRs(allMRs, oldTagDate, args.branch)
  
  print('Last tag %s was created at %s' % (args.lastTag, str(oldTagDate)))
  
  outputFile = open(args.output, 'w')
  
  outputFile.write('''DecFiles {0} {1} \n==========================  \n \n'''.format(args.newTag.strip(), str(datetime.date.today()).strip()))
  
  for mr in relevantMRs:
    formatMR(mr, outputFile)
  
  
  outputFile.close()
  
  
